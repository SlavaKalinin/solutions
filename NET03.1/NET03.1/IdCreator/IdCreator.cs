﻿using System;

namespace IdCreator
{
    public class IdCreator
    {
        private static IdCreator _instance;

        private static readonly object Locker = new object();

        private IdCreator()
        {
        }

        public static IdCreator GetInstance()
        {
            if (_instance != null)
            {
                return _instance;
            }

            lock (Locker)
            {
                _instance = new IdCreator();
            }

            return _instance;
        }

        public string Create()
        {
            return Guid.NewGuid().ToString();
        }
    }
}