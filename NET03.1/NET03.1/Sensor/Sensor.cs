﻿using System;
using System.Collections.Generic;
using Sensor.StateMode;

namespace Sensor
{
    public class Sensor : ISensor
    {
        private readonly List<ISensorUpdate> _observers = new List<ISensorUpdate>();
        private string _id;
        private int _interval;
        private double _value;

        public State SensState { get; set; }
        public SensorType TypeOfSensor { get; set; }

        public double Value
        {
            get => _value;
            set
            {
                _value = value;
                Notify();
            }
        }

        public string Id
        {
            get => _id;
            set
            {
                CheckArgs(value);
                _id = value;
            }
        }

        public int Interval
        {
            get => _interval;
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException(nameof(value));
                }

                _interval = value;
            }
        }

        public Sensor()
        {
            TransitionTo(new Idle());
            Id = IdCreator.IdCreator.GetInstance().Create();
        }

        public Sensor(State state)
        {
            TransitionTo(state);
            Id = IdCreator.IdCreator.GetInstance().Create();
        }

        public void Attach(ISensorUpdate sub)
        {
            if (sub is null)
            {
                return;
            }

            _observers.Add(sub);
        }

        public void Detach(ISensorUpdate sub)
        {
            _observers.Remove(sub);
        }

        public void Notify()
        {
            foreach (ISensorUpdate sensorUpdate in _observers)
            {
                sensorUpdate.Update(this);
            }
        }

        public void TransitionTo(State state)
        {
            SensState = state;
            SensState.SetState(this);
        }

        public void Mode()
        {
            SensState.NewMode();
            SensState.NewValue();
        }

        private void CheckArgs(object arg)
        {
            if (arg is null)
            {
                throw new ArgumentNullException(nameof(arg));
            }
        }
    }
}