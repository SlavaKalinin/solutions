﻿namespace Sensor
{
    public interface ISensorUpdate
    {
        void Update(ISensor iSensor);
    }
}