﻿namespace Sensor.StateMode
{
    public abstract class State
    {
        protected Sensor SensorSt;

        public void SetState(Sensor context)
        {
            SensorSt = context;
        }

        public abstract void NewMode();

        public abstract void NewValue();

        protected abstract void WorkingTimer(object ob);
    }
}