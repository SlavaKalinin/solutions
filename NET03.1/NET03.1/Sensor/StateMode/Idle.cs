﻿namespace Sensor.StateMode
{
    public class Idle : State
    {
        public override void NewMode()
        {
            SensorSt.TransitionTo(new Calibration());
        }

        public override void NewValue()
        {
            //doing nothing
        }

        protected override void WorkingTimer(object ob)
        {
            //doing nothing
        }
    }
}