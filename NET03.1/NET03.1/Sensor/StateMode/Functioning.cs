﻿using System;
using System.Threading;

namespace Sensor.StateMode
{
    public class Functioning : State
    {
        private Timer _timer;
        private TimerCallback _timerCallback;

        public override void NewMode()
        {
            _timer.Dispose();
            SensorSt.TransitionTo(new Idle());
        }

        public override void NewValue()
        {
            _timerCallback = WorkingTimer;
            _timer = new Timer(_timerCallback, null, 0, SensorSt.Interval);
        }

        protected override void WorkingTimer(object ob)
        {
            Random rand = new Random();

            double temp = Convert.ToDouble(rand.Next(1000)) / 10;
            SensorSt.Value = temp;
        }
    }
}