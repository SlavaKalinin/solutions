﻿using System.Threading;

namespace Sensor.StateMode
{
    public class Calibration : State
    {
        private Timer _timer;
        private TimerCallback _timerCallback;

        public override void NewMode()
        {
            _timer.Dispose();
            SensorSt.TransitionTo(new Functioning());
        }

        public override void NewValue()
        {
            _timerCallback = WorkingTimer;
            _timer = new Timer(_timerCallback, null, 0, 1000);
        }

        protected override void WorkingTimer(object obj)
        {
            SensorSt.Value++;
        }
    }
}