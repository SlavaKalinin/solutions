﻿namespace Sensor
{
    public interface ISensor
    {
        void Attach(ISensorUpdate observer);

        void Detach(ISensorUpdate observer);

        void Notify();
    }
}