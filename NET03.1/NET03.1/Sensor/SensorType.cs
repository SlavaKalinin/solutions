﻿namespace Sensor
{
    public enum SensorType
    {
        Default,
        Temperature,
        Pressure,
        MagneticField
    }
}