﻿namespace NET03._1.Model
{
    public abstract class State
    {
        protected Sensor SensorSt;

        public void SetContext(Sensor context)
        {
            SensorSt = context;
        }

        public abstract void Mode();

    }
}