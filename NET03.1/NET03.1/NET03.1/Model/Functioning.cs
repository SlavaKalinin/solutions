﻿using System;
using System.Threading;

namespace NET03._1.Model
{
    public class Functioning : State
    {
        private object obj = new object();
        private object obj2 = new object();

        public override void Mode()
        {
            
            TimerCallback timerCallback = TimeMake;
            Timer timer = new Timer(timerCallback, null, 0, SensorSt.Interval);
            lock (obj2)
            {
                SensorSt.TransitionTo(new Idle());
            }

        }
        private void TimeMake(object ob)
        {
            
                Random rand = new Random();

                var temp = Convert.ToDouble(rand.Next(1000)) / 10;
                SensorSt.Value = temp;
            
           
        }

    }
}