﻿namespace NET03._1.Model
{
    public enum SensorType
    {
        Default,
        Temperature,
        Pressure,
        MagneticField
    }
}