﻿using System;

namespace NET03._1.Model.Sensor
{
    public class IdCreator
    {
        private static IdCreator _instance;

        private static readonly object Locker = new object();

        public static IdCreator GetInstance()
        {
            if (_instance == null)
            {
                lock (Locker)
                {
                    _instance = new IdCreator();
                }
            }

            return _instance;
        }

        public string Create()
        {
            return Guid.NewGuid().ToString();
        }
    }
}