﻿namespace NET03._1.Model.Sensor
{
    public enum SensorType
    {
        Default,
        Temperature,
        Pressure,
        MagneticField
    }
}