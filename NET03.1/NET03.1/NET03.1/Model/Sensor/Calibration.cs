﻿using System.Threading;

namespace NET03._1.Model.Sensor
{
    public class Calibration : State
    {
        private Timer _timer;
        private TimerCallback _timerCallback;

        public override void NewMode()
        {
            _timer.Dispose();
            SensorSt.TransitionTo(new Functioning());
        }

        public override void NewValue()
        {
            _timerCallback = TimeMake;
            _timer = new Timer(_timerCallback, null, 0, 1000);
        }

        protected override void TimeMake(object obj)
        {
          
                SensorSt.Value++;
            
        }
    }
}