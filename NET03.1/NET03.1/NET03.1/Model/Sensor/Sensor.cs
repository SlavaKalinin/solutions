﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;

namespace NET03._1.Model.Sensor
{
    public class Sensor : INotifyPropertyChanged
    {
        private string _id;
        private int _interval;
        private double _value;

        public State SensState { private get; set; }
        public SensorType TypeOfSensor { get; set; }

        public double Value
        {
            get => _value;
            set
            {
                _value = value;
                OnPropertyChanged(nameof(Value));
            }
        }

        public string Id
        {
            get => _id;
            set
            {
                CheckArgs(value);
                _id = value;
                OnPropertyChanged(nameof(Id));
            }
        }

        public int Interval
        {
            get => _interval;
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException(nameof(value));
                }

                _interval = value;
                OnPropertyChanged(nameof(Interval));
            }
        }

        public Sensor()
        {
            TransitionTo(new Idle());
            Id = IdCreator.GetInstance().Create();
        }

        public Sensor(State state)
        {
            TransitionTo(state);
            Id = IdCreator.GetInstance().Create();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void TransitionTo(State state)
        {
            SensState = state;
            SensState.SetContext(this);
        }

        public void Mode()
        {
            SensState.NewMode();
            SensState.NewValue();
        }

        private void CheckArgs(object arg)
        {
            if (arg is null)
            {
                throw new ArgumentNullException(nameof(arg));
            }
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}