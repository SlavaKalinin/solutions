﻿namespace NET03._1.Model.Sensor
{
    public abstract class State
    {
        protected Sensor SensorSt;

        public void SetContext(Sensor context)
        {
            SensorSt = context;
        }

        public abstract void NewMode();

        public abstract void NewValue();

        protected abstract void TimeMake(object ob);
    }
}