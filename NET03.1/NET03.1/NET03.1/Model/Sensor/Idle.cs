﻿namespace NET03._1.Model.Sensor
{
    public class Idle : State
    {
        private object _obj = new object();

        public override void NewMode()
        {
            SensorSt.TransitionTo(new Calibration());
        }

        public override void NewValue()
        {
            //doing nothing
        }

        protected override void TimeMake(object ob)
        {
            //doing nothing
        }
    }
}