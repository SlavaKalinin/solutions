﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using ConsoleApp1.CreatingModelXmlJson;

namespace NET03._1.Model
{
    public class ApplicationViewModel : INotifyPropertyChanged
    {
        private Sensor _selectedSensor;
        private RelayCommand addCommand;
        private RelayCommand removeCommand;
        private RelayCommand doubleCommand;
        object objJ = new object();
        public RelayCommand DoubleCommand
        {
            get
            {
                lock (objJ)
                {
                    return doubleCommand ??
                           (doubleCommand = new RelayCommand(obj =>
                           {
                               Sensor phone = obj as Sensor;
                               Task.Factory.StartNew(() => phone.Mode());
                               //TaskScheduler.FromCurrentSynchronizationContext());

                               //Sensor sensor = obj as Sensor;
                               //sensor?.Mode();

                               // phone.Mode();

                           }));
                }
            }
        }
        public RelayCommand RemoveCommand
        {
            get
            {
                return removeCommand ??
                       (removeCommand = new RelayCommand(obj =>
                           {
                               Sensor sensor = obj as Sensor;
                               if (sensor != null)
                               {
                                   Sensors.Remove(sensor);
                               }
                           },
                           (obj) => Sensors.Count > 0));
            }
        }
        public RelayCommand AddCommand
        {
            get
            {
                return addCommand ??
                       (addCommand = new RelayCommand(obj =>
                       {
                           Sensor sensor = new Sensor();
                           Sensors.Insert(0, sensor);
                           SelectedSensor = sensor;
                       }));
            }
        }
        public ObservableCollection<Sensor> Sensors { get; set; }
        public Sensor SelectedSensor
        {
            get => _selectedSensor;
            set
            {
                _selectedSensor = value;
                OnPropertyChanged(nameof(SelectedSensor));
            }
        }

        public ApplicationViewModel()
        {
            Factory factory = new Xml();
            Sensors = factory.GetList();

        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}