﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using NET03._1.Model;

namespace ConsoleApp1.CreatingModelXmlJson
{
    public abstract class Factory
    {
        public abstract IReadLoad ReadLoad();

        public ObservableCollection<Sensor> GetList()
        {
            IReadLoad iReadLoad = ReadLoad();
            return iReadLoad.ReadDoc();
        }

        public void Save()
        {
            IReadLoad iReadLoad = ReadLoad();
            iReadLoad.WriteToDoc();
        }
    }
}