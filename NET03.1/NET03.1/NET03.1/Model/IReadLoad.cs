﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using NET03._1.Model;

namespace ConsoleApp1.CreatingModelXmlJson
{
    public interface IReadLoad
    {
        ObservableCollection<Sensor> ReadDoc();
        void WriteToDoc();
    }
}