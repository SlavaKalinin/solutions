﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Linq;
using NET03._1.Model;

namespace ConsoleApp1.CreatingModelXmlJson
{
    public class XmlWriterLoader : IReadLoad
    {
        public ObservableCollection<Sensor> ReadDoc()
        {

            {
                XDocument doc = XDocument.Load(@"D:\VS projects\ConsoleApp1\ConsoleApp1\SensorInfo.xml");
                ObservableCollection<Sensor> sensors = new ObservableCollection<Sensor>(doc.Element("config")
                    ?.Elements("sensor").Select(log => new Sensor()
                        {
                            TypeOfSensor = (SensorType)Enum.Parse(typeof(SensorType), log.Element("type")?.Value ?? throw new InvalidOperationException()),
                            Interval = int.Parse(log.Element("interval")?.Value ?? throw new InvalidOperationException())

                        }
                    ).ToList());
                return sensors;
                // Configuration.Logins = logins;
            }


        }

        public void WriteToDoc()
        {
            // dosmth
        }
    }
}