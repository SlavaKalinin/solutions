﻿namespace ConsoleApp1.CreatingModelXmlJson
{
    public class Xml : Factory
    {
        public override IReadLoad ReadLoad()
        {
            return new XmlWriterLoader();
        }
    }
}