﻿namespace ConsoleApp1.CreatingModelXmlJson
{
    public class Json : Factory
    {
        public override IReadLoad ReadLoad()
        {
            return new JsonWriterLoader();
        }
    }
}