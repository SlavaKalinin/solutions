﻿namespace NET03._1.Model.WriteRead
{
    public class Xml : Storage
    {
        public override IReadLoad ReadLoad()
        {
            return new XmlWriterLoader();
        }
    }
}