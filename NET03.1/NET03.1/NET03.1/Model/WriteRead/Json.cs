﻿namespace NET03._1.Model.WriteRead
{
    public class Json : Storage
    {
        public override IReadLoad ReadLoad()
        {
            return new JsonWriterLoader();
        }
    }
}