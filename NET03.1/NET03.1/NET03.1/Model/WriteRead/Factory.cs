﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Documents;

namespace NET03._1.Model.WriteRead
{
    public abstract class Factory
    {
        public abstract IReadLoad ReadLoad();

        public ObservableCollection<Sensor.Sensor> GetList(string name)
        {
            IReadLoad iReadLoad = ReadLoad();
            return iReadLoad.ReadDoc(name);
        }

        public void Save(string name, List<Sensor.Sensor> sList)
        {
            IReadLoad iReadLoad = ReadLoad();
            iReadLoad.WriteToDoc(name, sList);
        }
    }
}