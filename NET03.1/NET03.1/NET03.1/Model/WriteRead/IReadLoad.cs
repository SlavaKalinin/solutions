﻿using System.Collections.Generic;

namespace NET03._1.Model.WriteRead
{
    public interface IReadLoad
    {
        List<Sensor.Sensor> ReadDoc(string name);
        void WriteToDoc(string name, List<Sensor.Sensor> list);
    }
}