﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace NET03._1.Model.WriteRead
{
    public class JsonWriterLoader : IReadLoad
    {
        public List<Sensor.Sensor> ReadDoc(string name)
        {
            return new List<Sensor.Sensor>(JsonConvert.DeserializeObject<List<Sensor.Sensor>>
                                                               (File.ReadAllText(name)));
        }

        public void WriteToDoc(string name, List<Sensor.Sensor> sensorsList)
        {
             File.WriteAllText(name,
             JsonConvert.SerializeObject(sensorsList, Newtonsoft.Json.Formatting.Indented));
        }
    }
}