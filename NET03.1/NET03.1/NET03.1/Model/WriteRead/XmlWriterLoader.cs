﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Sensor;

namespace NET03._1.Model.WriteRead
{
    public class XmlWriterLoader : IReadLoad
    {
        public List<Sensor.Sensor> ReadDoc(string name)
        {
            {
                XDocument doc = XDocument.Load(name);
                List<Sensor.Sensor> sensors =
                    new List<Sensor.Sensor>
                    (doc.Element("config")
                         ?.Elements("sensor").Select(log => new Sensor.Sensor
                             {
                                 TypeOfSensor =
                                     (SensorType) Enum.Parse(typeof(SensorType),
                                         log.Element("type")?.Value ??
                                         throw new InvalidOperationException()),
                                 Interval = int.Parse(
                                     log.Element("interval")?.Value ??
                                     throw new InvalidOperationException()),
                                 Id = log.Element("id")?.Value
                             }
                         ).ToList() ?? throw new InvalidOperationException());
                return sensors;
            }
        }

        public void WriteToDoc(string name, List<Sensor.Sensor> list)
        {
            XElement xElement = new XElement("config");
            foreach (Sensor.Sensor sensor in list)
            {
                xElement.Add(new XElement("sensor",
                    new XElement("id", sensor.Id),
                    new XElement("interval", sensor.Interval),
                    new XElement("type", sensor.TypeOfSensor)));
            }

            XDocument docx = new XDocument(xElement);
            docx.Save(name);
        }
    }
}