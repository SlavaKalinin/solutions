﻿using System.Collections.Generic;

namespace NET03._1.Model.WriteRead
{
    public abstract class Storage
    {
        public abstract IReadLoad ReadLoad();

        public List<Sensor.Sensor> GetList(string name)
        {
            IReadLoad iReadLoad = ReadLoad();
            return iReadLoad.ReadDoc(name);
        }

        public void Save(string name, List<Sensor.Sensor> sList)
        {
            IReadLoad iReadLoad = ReadLoad();
            iReadLoad.WriteToDoc(name, sList);
        }
    }
}