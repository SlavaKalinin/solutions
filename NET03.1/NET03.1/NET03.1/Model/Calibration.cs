﻿using System.Threading;

namespace NET03._1.Model
{
    public class Calibration : State
    {
        private object obj = new object();
        private object obj2 = new object();

        public override void Mode()
        {
            
            TimerCallback timerCallback = TimeMake;
            Timer timer = new Timer(timerCallback, null, 0, 10);
            lock (obj2)
            {
                SensorSt.TransitionTo(new Functioning());

            }

        }

        private void TimeMake(object objec)
        {
            lock (obj)
            {
                SensorSt.Value++;
            }
        }

       
    }
}