﻿using System;

namespace NET03._1.Model
{
    public class IdCreator
    {
        private static IdCreator _instance;

        private static readonly object locker = new object();

        public static IdCreator GetInstance()
        {
            if (_instance == null)
            {
                lock (locker)
                {
                    _instance = new IdCreator();
                }
            }

            return _instance;
        }

        public string Create() => Guid.NewGuid().ToString();
    }
}