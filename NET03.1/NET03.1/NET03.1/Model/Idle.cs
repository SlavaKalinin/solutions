﻿using System.Threading;
using System.Windows;

namespace NET03._1.Model
{
    public class Idle : State
    {
        private object obj = new object();
        public override void Mode()
        {
            lock (obj)
            {
                SensorSt.TransitionTo(new Calibration());
            }

            //doing nothing
        }


    }
}