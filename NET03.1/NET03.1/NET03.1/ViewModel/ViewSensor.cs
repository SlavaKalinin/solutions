﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Sensor;
using Sensor.StateMode;

namespace NET03._1.ViewModel
{
    public class ViewSensor : ISensorUpdate, INotifyPropertyChanged
    {
        public Sensor.Sensor VSensor { get; set; }

        public State SensState
        {
            private get { return VSensor.SensState; }
            set { VSensor.SensState = value; }
        }

        public SensorType TypeOfSensor
        {
            get => VSensor.TypeOfSensor;
            set
            {
                VSensor.TypeOfSensor = value;
                OnPropertyChanged(nameof(TypeOfSensor));
            }
        }

        public double Value
        {
            get => VSensor.Value;
            set => VSensor.Value = value;
        }

        public string Id
        {
            get => VSensor.Id;
            set
            {
                VSensor.Id = value;
                OnPropertyChanged(nameof(Id));
            }
        }

        public int Interval
        {
            get => VSensor.Interval;
            set
            {
                VSensor.Interval = value;
                OnPropertyChanged(nameof(Interval));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ViewSensor(Sensor.Sensor vSensor)
        {
            VSensor = vSensor;
            TransitionTo(new Idle());
            VSensor.Attach(this);
            Id = IdCreator.IdCreator.GetInstance().Create();
        }

        public void Update(ISensor iSensor)
        {
            OnPropertyChanged(nameof(Value));
        }

        public void TransitionTo(State state)
        {
            SensState = state;
            SensState.SetState(VSensor);
        }

        public void Mode()
        {
            VSensor.Mode();
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}