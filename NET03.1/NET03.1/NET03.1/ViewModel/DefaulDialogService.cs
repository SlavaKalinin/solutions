﻿using System.Windows;
using Microsoft.Win32;

namespace NET03._1.ViewModel
{
    public class DefaulDialogService : IDialogService
    {

        public string FilePath { get; set; }

        public bool OpenFileDialog()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                openFileDialog.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*|JSON files (*.json)|*.json|All files (*.*)|*.*";
                FilePath = openFileDialog.FileName;
                return true;
            }
            return false;
        }

        public bool SaveFileDialog()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            if (saveFileDialog.ShowDialog() == true)
            {
                saveFileDialog.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*|JSON files (*.json)|*.json|All files (*.*)|*.*";
                FilePath = saveFileDialog.FileName;
                return true;
            }
            return false;
        }

        public void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }
    }

}