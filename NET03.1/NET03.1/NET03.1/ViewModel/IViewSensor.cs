﻿using System;
using Sensor;

namespace NET03._1.ViewModel
{
    public interface IViewSensor
    {
        void Attach(ISensorUpdate observer);

        // Отсоединяет наблюдателя от издателя.
        void Detach(ISensorUpdate observer);

        // Уведомляет всех наблюдателей о событии.
        void Notify();
    }
}