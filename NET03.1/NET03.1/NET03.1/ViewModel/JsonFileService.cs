﻿using System.Collections.Generic;
using System.IO;
using NET03._1.Model.Sensor;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace NET03._1.ViewModel
{
    public class JsonFileService : IFileService
    {
        public List<Sensor> Open(string filename)
        {
            List<Sensor> phones = new List<Sensor>();
            DataContractJsonSerializer jsonFormatter =
                new DataContractJsonSerializer(typeof(List<Sensor>));
            using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate))
            {
                phones = jsonFormatter.ReadObject(fs) as List<Sensor>;
            }

            return phones;
        }

        public void Save(string filename, List<Sensor> phonesList)
        {
            DataContractJsonSerializer jsonFormatter =
                new DataContractJsonSerializer(typeof(List<Sensor>));
            using (FileStream fs = new FileStream(filename, FileMode.Create))
            {
                jsonFormatter.WriteObject(fs, phonesList);
            }
        }
    }
}