﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using NET03._1.Model.WriteRead;

namespace NET03._1.ViewModel
{
    public class ApplicationViewModel : INotifyPropertyChanged
    {
        private readonly DefaultDialogService _dialogService = new DefaultDialogService();
        private readonly object _obj = new object();
        private RelayCommand _addCommand;
        private RelayCommand _modeCommand;
        private RelayCommand _openCommand;
        private RelayCommand _removeCommand;
        private RelayCommand _saveCommand;
        private ViewSensor _selectedSensor;

        public ObservableCollection<ViewSensor> Sensors { get; set; }

        public ViewSensor SelectedSensor
        {
            get => _selectedSensor;
            set
            {
                _selectedSensor = value;
                OnPropertyChanged(nameof(SelectedSensor));
            }
        }

        public ApplicationViewModel()
        {
            Sensors = new ObservableCollection<ViewSensor>();
        }

        public RelayCommand SaveCommand
        {
            get
            {
                return _saveCommand ?? (_saveCommand = new RelayCommand(obj =>
                {
                    Storage factory = null;
                    try
                    {
                        _dialogService.SaveFileDialog();
                        if (_dialogService.FilePath.Contains(".xml"))
                        {
                            factory = new Xml();
                        }

                        if (_dialogService.FilePath.Contains(".json"))
                        {
                            factory = new Json();
                        }

                        List<Sensor.Sensor> sens = new List<Sensor.Sensor>();
                        foreach (ViewSensor sensor in Sensors)
                        {
                            sens.Add(sensor.VSensor);
                        }
                        factory?.Save(_dialogService.FilePath, sens);
                        MessageBox.Show("File has been saved");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }));
            }
        }

        public RelayCommand OpenCommand
        {
            get
            {
                return _openCommand ?? (_openCommand = new RelayCommand(obj =>
                       {
                           Storage factory = null;
                           try
                           {
                               _dialogService.OpenFileDialog();

                               if (_dialogService.FilePath.Contains(".xml"))
                               {
                                   factory = new Xml();
                               }

                               if (_dialogService.FilePath.Contains(".json"))
                               {
                                   factory = new Json();
                               }

                               List<Sensor.Sensor> sensors = factory?.GetList(_dialogService.FilePath);
                               Sensors.Clear();
                               if (sensors != null)
                               {
                                   foreach (Sensor.Sensor sensor in sensors)
                                   {
                                       Sensors.Add(new ViewSensor(sensor));
                                   }
                               }

                               MessageBox.Show("File is opened successfully");
                           }
                           catch (Exception ex)
                           {
                               MessageBox.Show(ex.Message);
                           }
                       }));
            }
        }

        public RelayCommand ModeCommand
        {
            get
            {
                lock (_obj)
                {
                    return _modeCommand ?? (_modeCommand = new RelayCommand(obj =>
                           {
                               ViewSensor sensor = obj as ViewSensor;
                               Task.Factory.StartNew(() => sensor?.Mode());
                           }));
                }
            }
        }

        public RelayCommand RemoveCommand
        {
            get
            {
                return _removeCommand ?? (_removeCommand = new RelayCommand(obj =>
                           {
                               if (obj is ViewSensor sensor)
                               {
                                   Sensors.Remove(sensor);
                               }
                           },
                           obj => Sensors.Count > 0));
            }
        }

        public RelayCommand AddCommand
        {
            get
            {
                return _addCommand ?? (_addCommand = new RelayCommand(obj =>
                       {
                           ViewSensor sensor = new ViewSensor(new Sensor.Sensor());
                           Sensors.Insert(0, sensor);
                           SelectedSensor = sensor;
                       }));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}