﻿using System.Collections.Generic;
using NET03._1.Model.Sensor;

namespace NET03._1.ViewModel
{
    public interface IFileService
    {
        List<Sensor> Open(string filename);
        void Save(string filename, List<Sensor> phonesList);
    }
}