﻿using System;
using Microsoft.Win32;

namespace NET03._1.ViewModel
{
    public class DefaultDialogService
    {
        private const string Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*|JSON files (*.json)|*.json|All files (*.*)|*.*";

        private readonly OpenFileDialog _openFileDialog = new OpenFileDialog();
        private readonly SaveFileDialog _saveFileDialog = new SaveFileDialog();

        public DefaultDialogService()
        {
            _saveFileDialog.Filter = Filter;
            _openFileDialog.Filter = Filter;
        }

        public string FilePath { get; set; }

        public void OpenFileDialog()
        {
            if (_openFileDialog.ShowDialog() == true)
            {
                FilePath = _openFileDialog.FileName;
                return;
            }

            throw new ArgumentException();
        }

        public void SaveFileDialog()
        {
            if (_saveFileDialog.ShowDialog() == true)
            {
                FilePath = _saveFileDialog.FileName;
                return;
            }

            throw new ArgumentException();
        }
    }
}