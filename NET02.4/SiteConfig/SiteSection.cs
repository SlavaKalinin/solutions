﻿using System.Configuration;

namespace SiteConfig
{
    public class SiteSection : ConfigurationSection
    {
        [ConfigurationProperty("", IsDefaultCollection = true)]
        public SiteCollection Sites
        {
            get
            {
                SiteCollection siteCollection = (SiteCollection) base[""];
                return siteCollection;
            }
        }
    }
}