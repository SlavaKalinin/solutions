﻿using System;
using System.Net;
using System.Net.Mail;

namespace SiteConfig
{
    public static class SendingMessage
    {
        public static async void SendMessageAsync(Site site, string message, string mailSender, string mailReceiving,
            string mailSenderPass)
        {
            CheckArgs(site);
            CheckArgs(message);
            CheckArgs(mailSender);
            CheckArgs(mailReceiving);
            CheckArgs(mailSenderPass);

            MailAddress from = new MailAddress(mailSender);
            MailAddress to = new MailAddress(mailReceiving);
            MailMessage mailMessage = new MailMessage(from, to)
            {
                Subject = $"{site.Name} is not available",
                Body = $"cannot get access to {site.Name} + {message}"
            };
            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential(mailSender, mailSenderPass),
                EnableSsl = true
            };

            await smtp.SendMailAsync(mailMessage);
        }

        private static void CheckArgs(object obj)
        {
            if (obj is null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
        }
    }
}