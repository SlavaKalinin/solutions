﻿using System.Configuration;

namespace SiteConfig
{
    public class SiteConfigElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get => (string) this["name"];
            set => this["name"] = value;
        }

        [ConfigurationProperty("interval", IsRequired = true)]
        public int Interval
        {
            get => (int) this["interval"];
            set => this["interval"] = value;
        }

        [ConfigurationProperty("expectation", IsRequired = true, DefaultValue = 22)]
        [IntegerValidator(MinValue = 1, MaxValue = 65536)]
        public int Expectation
        {
            get => (int) this["expectation"];
            set => this["expectation"] = value;
        }

        [ConfigurationProperty("email", IsRequired = false)]
        public string Email
        {
            get => (string) this["email"];
            set => this["email"] = value;
        }
    }
}