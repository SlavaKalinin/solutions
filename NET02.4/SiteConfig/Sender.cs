﻿using System;

namespace SiteConfig
{
    public class Sender
    {
        private string _emailSender;
        private string _passwordSender;

        public string EmailSender
        {
            get => _emailSender;
            set
            {
                CheckArgs(value);
                _emailSender = value;
            }
        }

        public string PasswordSender
        {
            get => _passwordSender;
            set
            {
                CheckArgs(value);
                _passwordSender = value;
            }
        }

        private void CheckArgs(string arg)
        {
            if (arg is null)
            {
                throw new ArgumentNullException(nameof(arg));
            }
        }
    }
}