﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading;

namespace SiteConfig
{
    public class Monitoring
    {
        private string _logSection;
        private Sender _sender;
        private string _siteSection;
        public List<Site> Sites { get; private set; }

        public string SiteSection
        {
            get => _siteSection;
            set
            {
                CheckArgs(value);
                _siteSection = value;
            }
        }

        public string LogSection
        {
            get => _logSection;
            set
            {
                CheckArgs(value);
                _logSection = value;
            }
        }

        public Sender SenderMessages
        {
            get => _sender;
            set
            {
                CheckArgs(value);
                _sender = value;
            }
        }

        public void Start()
        {
            Sites = ReadExeConfig.ReadExe(SiteSection, LogSection);
            FileWatcher();
        }

        public void TestSites(object url)
        {
            Site site = (Site)url;
            Console.WriteLine(site.Name);
            site.TestSite(SenderMessages);
        }

        private void Update()
        {
            ConfigurationManager.RefreshSection(SiteSection);
            ConfigurationManager.RefreshSection(LogSection);
            Sites = ReadExeConfig.ReadExe(SiteSection, LogSection);

            foreach (Site site in Sites)
            {
                TimerCallback tm = TestSites;
                Timer timer = new Timer(tm, site, 0, site.Interval);
            }
        }

        private void FileWatcher()
        {
            Update();
            FileInfo appConfig = new FileInfo("NET02.4.exe.config");

            FileSystemWatcher systemWatcher = new FileSystemWatcher
            {
                Path = appConfig.DirectoryName,
                NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName |
                               NotifyFilters.DirectoryName,
                Filter = "*.config"
            };
            systemWatcher.Changed += (o, e) => Update();
            systemWatcher.EnableRaisingEvents = true;
        }

        private void CheckArgs(object obj)
        {
            if (obj is null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
        }
    }

    //password 12345678_k!KSSa_!11
}