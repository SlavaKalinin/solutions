﻿using System.Configuration;

namespace SiteConfig
{
    public class SiteCollection : ConfigurationElementCollection
    {
        public SiteCollection()
        {
            SiteConfigElement details = (SiteConfigElement) CreateNewElement();
            if (details.Name != "")
            {
                Add(details);
            }
        }

        public override ConfigurationElementCollectionType CollectionType =>
            ConfigurationElementCollectionType.BasicMap;

        public SiteConfigElement this[int index]
        {
            get => (SiteConfigElement) BaseGet(index);
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }

                BaseAdd(index, value);
            }
        }

        public new SiteConfigElement this[string name] => (SiteConfigElement) BaseGet(name);

        protected override string ElementName => "site";

        protected sealed override ConfigurationElement CreateNewElement()
        {
            return new SiteConfigElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SiteConfigElement) element).Name;
        }


        public void Add(SiteConfigElement details)
        {
            BaseAdd(details);
        }

        protected override void BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
        }
    }
}