﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace SiteConfig
{
    public class ReadExeConfig
    {
        public static List<Site> ReadExe(string siteSection, string logConfig)
        {
            CheckArgs(siteSection);
            CheckArgs(logConfig);
            ConfigurationManager.RefreshSection(siteSection);
            ConfigurationManager.RefreshSection(logConfig);
            SiteSection sites = (SiteSection) ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
                .GetSection(siteSection);
            CheckArgs(sites);
            List<Site> siteList = new List<Site>();
            for (int i = 0; i < sites.Sites.Count; i++)
            {
                siteList.Add(new Site
                {
                    Expectation = sites.Sites[i].Expectation,
                    Interval = sites.Sites[i].Interval,
                    Email = sites.Sites[i].Email,
                    Name = sites.Sites[i].Name
                });
            }

            return siteList;
        }

        private static void CheckArgs(object arg)
        {
            if (arg is null)
            {
                throw new ArgumentNullException(nameof(arg));
            }
        }
    }
}