﻿using System.Collections.Generic;
using System.Text;

namespace NET02._2
{
    public class Config
    {
        public List<Login> Logins { get; set; } = new List<Login>();

        public List<Login> InCorrectLogins()
        {
            List<Login> logins = new List<Login>();
            foreach (Login login in Logins)
            {
                if (login.Windows == null || login.IsCorrect())
                {
                    continue;
                }

                logins.Add(login);
            }

            return logins;
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            foreach (Login login in Logins)
            {
                if (login.Windows == null)
                {
                    continue;
                }

                str.Append(login);
            }

            return str.ToString();
        }
    }
}