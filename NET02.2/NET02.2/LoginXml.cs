﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace NET02._2
{
    public class LoginXml
    {
        private static readonly DirectoryInfo Dir =
            new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));

        public Config ConfigName { get; set; }

        public LoginXml(Config config)
        {
            ConfigName = config ?? throw new ArgumentNullException(nameof(config));
            CreateDir();
            MakeXmlLogins();
        }

        private static void CreateDir()
        {
            DirectoryInfo subDir =
                new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Config");
            if (!subDir.Exists)
            {
                Dir.CreateSubdirectory("Config");
            }
        }

        private void MakeXmlLogins()
        {
            foreach (Login configsLogin in ConfigName.Logins)
            {
                configsLogin.MakeStandardWindow();
                XDocument doc = new XDocument(new XElement("login",
                                        new XAttribute("name", configsLogin.Name),
                                        configsLogin.Windows.Select(p =>
                                            new XElement("window",
                                                new XAttribute("title", p.Title),
                                                new XElement("width", p.Width),
                                                new XElement("top", p.Top),
                                                new XElement("height", p.Height),
                                                new XElement("left", p.Left)))));
                doc.Save(Dir + "\\Config\\" + configsLogin.Name + ".xml");
            }
        }
    }
}