﻿using System;
using Newtonsoft.Json;

namespace NET02._2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            XmlParser xmlParser = new XmlParser("../../config.xml");
            Console.WriteLine(xmlParser.Configuration.ToString());
            Config config = xmlParser.Configuration;
            foreach (Login login in config.InCorrectLogins())
            {
                Console.WriteLine(login);
            }

            LoginXml serializer = new LoginXml(config);

            string jsonData = JsonConvert.SerializeObject(config, Formatting.Indented);
            Console.WriteLine(jsonData);
            SerializerConfigJson serializers = new SerializerConfigJson(config, "config.json");

            Console.ReadLine();
        }
    }
}