﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace NET02._2
{
    public class XmlParser
    {
        public Config Configuration { get; } = new Config();

        public string NameDoc { get; }

        public XmlParser(string nameDoc)
        {
            NameDoc = nameDoc ?? throw new ArgumentNullException(nameof(nameDoc));
            ReadXmlDocLinq();
        }

        private void ReadXmlDocLinq()
        {
            XDocument doc = XDocument.Load(NameDoc);
            List<Login> logins = doc.Element("config")
                ?.Elements("login").Select(log => new Login()
                    {
                        Name = log.Attribute("name")?.Value,
                        Windows = log.Elements("window").Select(wind => new Window
                        {
                            Title = wind.Attribute("title")?.Value,
                            Height = ParseNullable(wind.Element("height")?.Value),
                            Left = ParseNullable(wind.Element("left")?.Value),
                            Top = ParseNullable(wind.Element("top")?.Value),
                            Width = ParseNullable(wind.Element("width")?.Value)
                        }).ToList()
                    }
                ).ToList();
            Configuration.Logins = logins;
        }

        private int? ParseNullable(string value)
        {
            if (value == null)
            {
                return null;
            }

            return int.Parse(value);
        }
    }
}