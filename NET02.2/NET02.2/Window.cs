﻿using System.Text;

namespace NET02._2
{
    public class Window
    {
        public string Title { get; set; }
        public int? Top { get; set; }
        public int? Left { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }

        public bool IsCorrect()
        {
            return Title == "main" && !(Top is null || Left is null || Height is null || Width is null);
        }

        public void ChangeValues()
        {
            Top = Top ?? 0;
            Left = Left ?? 0;
            Height = Height ?? 150;
            Width = Width ?? 400;
        }

        public override string ToString()
        {
            return $"\t{Title} ({ValueOrNull(Top)}, {ValueOrNull(Left)}, {ValueOrNull(Height)}, {ValueOrNull(Width)})";
        }

        private string ValueOrNull(int? str)
        {
            return str == null ? "?" : str.ToString();
        }
    }
}