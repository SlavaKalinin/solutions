﻿using System.Collections.Generic;
using System.Text;

namespace NET02._2
{
    public class Login
    {
        public string Name { get; set; }
        public List<Window> Windows { get; set; } = new List<Window>();

        public bool IsCorrect()
        {
            foreach (Window window in Windows)
            {
                if (window.IsCorrect())
                {
                    return true;
                }
            }

            return false;
        }

        public void MakeStandardWindow()
        {
            foreach (Window window in Windows)
            {
                window.ChangeValues();
            }
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine($"Login: {Name}");
            foreach (Window window in Windows)
            {
                str.AppendLine(window.ToString());
            }

            return str.ToString();
        }
    }
}