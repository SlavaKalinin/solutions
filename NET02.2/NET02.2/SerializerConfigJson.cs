﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace NET02._2
{
    public class SerializerConfigJson
    {
        private static readonly DirectoryInfo Dir =
            new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
        public Config ConfigJson { get; }
        public string NameFile { get; }

        public SerializerConfigJson(Config config, string name)
        {
            if (config == null)
            {
                throw new ArgumentNullException(nameof(config));
            }

            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            config.Logins.ForEach(x => x.MakeStandardWindow());
            ConfigJson = config;
            NameFile = name;
            CreateDir();
            SerializeToJson();
        }

        private static void CreateDir()
        {
            DirectoryInfo subDir =
                new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\ConfigJson");
            if (!subDir.Exists)
            {
                Dir.CreateSubdirectory("ConfigJson");
            }
        }

        private void SerializeToJson()
        {
            File.WriteAllText(Dir + $"\\ConfigJson\\{NameFile}",
                JsonConvert.SerializeObject(ConfigJson, Formatting.Indented));
        }
    }
}