﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NET02._2.Tests
{
    [TestClass]
    public class WindowTests
    {
        [TestMethod]
        public void ChangeValue_Top_Is_True()
        {
            Window window = new Window()
            {
                Height = 10,
                Left = 20,
                Title = "main",
                Top = null,
                Width = null
            };
            window.ChangeValues();
            Assert.AreEqual(window.Top, 0);
        }

        [TestMethod]
        public void ChangeValue_Height_Is_True()
        {
            Window window = new Window()
            {
                Height = null,
                Left = 20,
                Title = "main",
                Top = null,
                Width = null
            };
            window.ChangeValues();
            Assert.AreEqual(window.Height, 150);
        }

        [TestMethod]
        public void IsCorrect_Title_Is_Main_true()
        {
            Window window = new Window()
            {
                Height = 10,
                Left = 20,
                Title = "main",
                Top = 10,
                Width = 10
            };
            Assert.IsTrue(window.IsCorrect());
        }

        [TestMethod]
        public void IsCorrect_Height_Is_Question_true()
        {
            Window window = new Window()
            {
                Height = null,
                Left = 20,
                Title = "main",
                Top = 10,
                Width = 10
            };
            Assert.IsFalse(window.IsCorrect());
        }
        
    }
}