﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NET02._2.Tests
{
    [TestClass]
    public class XmlParserTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NameDoc_Set_Null()
        {
            XmlParser xmlParser = new XmlParser(null);
        }
    }
}
