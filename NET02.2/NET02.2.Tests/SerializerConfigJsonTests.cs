﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NET02._2.Tests
{
    [TestClass]
    public class SerializerConfigJsonTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Config_Name_Set_Null()
        {
            SerializerConfigJson serializerConfig = new SerializerConfigJson(null, "name");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void File_Name_Set_Null()
        {
            SerializerConfigJson serializerConfig = new SerializerConfigJson(new Config(), null);
        }
    }
}