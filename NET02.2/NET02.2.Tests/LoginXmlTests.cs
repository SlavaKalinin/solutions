﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NET02._2.Tests
{
    [TestClass]
    public class LoginXmlTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Config_Name_Set_Null()
        {
            LoginXml serializerLogin = new LoginXml(null);
        }
    }
}