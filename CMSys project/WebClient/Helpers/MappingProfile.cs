﻿using System;
using System.Collections;
using System.Collections.Generic;
using AutoMapper;
using CMSys_project.ViewModels;
using Domain.Entities.CourseEntities;
using Domain.Entities.UserEntities;

namespace CMSys_project.Helpers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<CourseGroup, CourseGroupVM>();
            CreateMap<CourseGroupVM, CourseGroup>();
            CreateMap<TrainerGroupVM, TrainerGroup>();
            CreateMap<TrainerGroup, TrainerGroupVM>();
            CreateMap<CourseType, CourseTypeVM>();
            CreateMap<CourseTypeVM, CourseType>();
            CreateMap<WayPlanning, WayPlanningVM>();
            CreateMap<WayPlanningVM, WayPlanning>();
            CreateMap<TrainerVM, Trainer>();
            CreateMap<Role, RoleVM>();
            CreateMap<RoleVM, Role>();
            CreateMap<Trainer, TrainerVM>();
            CreateMap<Course, CourseVM>()
                .ForMember(z => z.Status, opt => opt.MapFrom(c => c.IsNew))
                .ForMember(z => z.Schedule, opt => opt.MapFrom(c => c.WayPlanning))
                .ForMember(z => z.Trainers, opt => opt.MapFrom(c => GetTrainers(c.TrainerCourse)));
            CreateMap<CourseVM, Course>()
                .ForMember(z => z.WayPlanning, opt => opt.MapFrom(c => c.Schedule))
                .ForMember(z => z.IsNew, opt => opt.MapFrom(c => c.Status));
            CreateMap<User, UserVM>()
                .ForMember(z => z.FullName, opt => opt.MapFrom(c => c.Name + " " + c.Surname))
                .ForMember(z => z.WorkEnd, opt => opt.MapFrom(c => c.WorkEnd))
                .ForMember(z => z.RoleVm, opt => opt.MapFrom(c => GetRoles(c.UserRole)));
            CreateMap<UserVM, User>()
                .ForMember(z => z.Name, opt => opt.MapFrom(c => GetName(c.FullName)))
                .ForMember(z => z.Surname, opt => opt.MapFrom(c => GetSurname(c.FullName)))
                .ForMember(z => z.WorkEnd, opt => opt.MapFrom(c => c.WorkEnd))
                .ForMember(z => z.UserRole, opt => opt.MapFrom(c => GetChangedRoles(c.RoleVm, c.Id)));
        }
        private string GetName(string str)
        {
            return str.Split(' ')[0];
        }

        private string GetSurname(string str)
        {
            return str.Split(' ')[1];
        }

        private List<Trainer> GetTrainers(ICollection<TrainerCourse> trainer)
        {
            List<Trainer> trainers = new List<Trainer>();
            foreach (TrainerCourse trainerCourse in trainer)
            {
                trainers.Add(trainerCourse.Trainer);
            }

            return trainers;
        }

        private List<Role> GetRoles(ICollection<UserRole> role)
        {
            List<Role> roles = new List<Role>();
            foreach (UserRole userRole in role)
            {
                roles.Add(userRole.Role);
            }

            return roles;
        }

        private ICollection<UserRole> GetChangedRoles(List<RoleVM> role, Guid id)
        {
            ICollection<UserRole> userRoles = new List<UserRole>();
            foreach (RoleVM roleVm in role)
            {
                userRoles.Add(new UserRole()
                {
                    UserId = id,
                    RoleId = roleVm.RoleId,
                    User = null,
                    Role = null
                });
            }

            return userRoles;
        }
    }
}