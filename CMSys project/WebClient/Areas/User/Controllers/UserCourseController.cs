﻿using System;
using System.Collections.Generic;
using AutoMapper;
using BLL.Contract;
using CMSys_project.ViewModels;
using Domain.Entities.CourseEntities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys_project.Areas.User.Controllers
{
    [Area("User")]
    [Authorize(Roles = "User, Administrator")]
    [Route("")]
    [Route("User/Course")]
    public class UserCourseController : Controller
    {
        private readonly ICatalogService _iCatalogService;
        private readonly IMapper _iMapper;

        public UserCourseController(ICatalogService iCatalogService, IMapper iMapper)
        {
            _iCatalogService = iCatalogService;
            _iMapper = iMapper;
        }
        [Route("")]
        [Route("Info")]
        public IActionResult Info()
        {
            IEnumerable<CourseVM> courseGroupVms =
                _iMapper.Map<IEnumerable<Course>, IEnumerable<CourseVM>>(_iCatalogService.GetAllCourse());
            return View("Info", courseGroupVms);

        }

        [Route("Error")]
        public IActionResult Error()
        {
            return View("Error");
        }

        [Route("Profile/{id}")]
        public IActionResult Profile(Guid id)
        {

            Course course = _iCatalogService.GetCourse(id);
            ViewData.Model = _iMapper.Map<Course, CourseVM>(course);
            if (course is null)
            {
                ViewBag.Message = "Course was not found";
                return View("Error");
            }

            return View();
        }

    }
}