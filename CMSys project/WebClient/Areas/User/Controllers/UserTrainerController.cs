﻿using System;
using System.Collections.Generic;
using AutoMapper;
using BLL.Contract;
using CMSys_project.ViewModels;
using Domain.Entities.UserEntities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys_project.Areas.User.Controllers
{
    [Area("User")]
    [Authorize(Roles = "User, Administrator")]
    [Route("User/Trainer")]
    public class UserTrainerController : Controller
    {
        private readonly ICatalogService _iCatalogService;
        private readonly IMapper _iMapper;

        public UserTrainerController(ICatalogService iCatalogService, IMapper iMapper)
        {
            _iCatalogService = iCatalogService;
            _iMapper = iMapper;
        }

        [Route("Info")]
        public IActionResult Info()
        {
            IEnumerable<TrainerVM> trainerVMs =
                _iMapper.Map<IEnumerable<Trainer>, IEnumerable<TrainerVM>>(_iCatalogService.GetAllTrainer());
           
            return View("Info", trainerVMs);
        }

 
        [Route("Profile/{id}")]
        public IActionResult Profile(Guid id)
        {

            Trainer trainer = _iCatalogService.GetTrainer(id);
            ViewData.Model = _iMapper.Map<Trainer, TrainerVM>(trainer);
            if (trainer is null)
            {
                ViewBag.Message = "Trainer was not found";
                return View("Error");
            }
            return View();

        }
    }
}