﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AutoMapper;
using BLL.Contract;
using CMSys_project.ViewModels;
using Domain.Entities.UserEntities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CMSys_project.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/User")]
    [Authorize(Roles = "Administrator")]
    public class AdminUserController : Controller
    {
        private readonly IMapper _iMapper;
        private readonly IMembershipService _iMembershipService;

        public AdminUserController(IMembershipService iMembershipService, IMapper iMapper)
        {
            _iMembershipService = iMembershipService;
            _iMapper = iMapper;
        }

        [Route("Info")]
        public IActionResult Info()
        {
            IEnumerable<UserVM> trainerVMs =
                _iMapper.Map<IEnumerable<Domain.Entities.UserEntities.User>, IEnumerable<UserVM>>(_iMembershipService
                    .GetAllUsers());
            return View("Info", trainerVMs);
        }

        [Route("Edit/{id}")]
        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            try
            {
                Domain.Entities.UserEntities.User user = _iMembershipService.GetUser(id);
                UserVM userVm = _iMapper.Map<Domain.Entities.UserEntities.User, UserVM>(user);
                List<RoleVM> allRoles = _iMapper
                    .Map<IEnumerable<Role>, IEnumerable<RoleVM>>(_iMembershipService.GetAllRoles()).ToList();

                List<RoleVM> aList = new List<RoleVM>();
                foreach (RoleVM roleVm in allRoles)
                {
                    RoleVM role = new RoleVM
                    {
                        Name = roleVm.Name,
                        Selected = userVm.RoleVm.Select(x => x.Name).Contains(roleVm.Name),
                        RoleId = roleVm.RoleId
                    };
                    aList.Add(role);
                }

                userVm.RoleVm = aList;

                return View(userVm);
            }
            catch
            {
                ViewBag.Message = "User was not found";
                return View("Error");
            }
        }

        [Route("Edit/{id}")]
        [HttpPost]
        public IActionResult Edit(UserVM userVm, IFormFile photo)
        {
            Domain.Entities.UserEntities.User user = _iMembershipService.GetUser(userVm.Id);
            user.WorkEnd = userVm.WorkEnd;
            List<UserRole> userRole = new List<UserRole>();
            foreach (RoleVM roleVm in userVm.RoleVm.Where(x => x.Selected.Equals(true)))
            {
                userRole.Add(new UserRole
                {
                    RoleId = roleVm.RoleId,
                    UserId = user.Id,
                    User = user,
                    Role = _iMembershipService.GetRole(roleVm.RoleId)
                });
            }

            userRole.Add(
                new UserRole
                {
                    RoleId = _iMembershipService.GetAllRoles().First(x => x.Name.Equals("User")).RoleId,
                    Role = _iMembershipService.GetAllRoles().First(x => x.Name.Equals("User")),
                    User = user,
                    UserId = user.Id
                }
            );
            if (photo != null && photo.Length > 0)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    photo.CopyTo(ms);
                    user.Photo = ms.ToArray();
                }
            }

            try
            {
                _iMembershipService.EditUser(user, userRole);
                return RedirectToAction("Info");
            }
            catch (Exception)
            {
                ViewBag.Message = "User was not found";
                return View("Error");
            }
        }
    }
}