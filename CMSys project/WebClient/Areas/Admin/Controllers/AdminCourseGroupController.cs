﻿using System;
using System.Collections.Generic;
using AutoMapper;
using BLL.Contract;
using CMSys_project.ViewModels;
using Domain.Entities.CourseEntities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys_project.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/CourseGroup")]
    [Authorize(Roles = "Administrator")]
    public class AdminCourseGroup : Controller
    {
        private readonly ICatalogService _iCatalogService;
        private readonly IMapper _iMapper;

        public AdminCourseGroup(ICatalogService iCatalogService, IMapper iMapper)
        {
            _iCatalogService = iCatalogService;
            _iMapper = iMapper;
        }

        [Route("Info")]
        public IActionResult Info()
        {
            IEnumerable<CourseGroupVM> courseGroupVms =
               _iMapper.Map<IEnumerable<CourseGroup>, IEnumerable<CourseGroupVM>>(_iCatalogService.GetAllCourseGroup());
            return View("Info", courseGroupVms);
        }

        [Route("Create")]
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [Route("Create")]
        [HttpPost]
        public IActionResult Create(CourseGroupVM courseGroup)
        {
            if (!ModelState.IsValid)
            {
                return View(courseGroup);
            }
            courseGroup.Id = Guid.NewGuid();
            CourseGroup courseGr = _iMapper.Map<CourseGroupVM, CourseGroup>(courseGroup);
            try
            {
                _iCatalogService.AddCourseGroup(courseGr);
                return RedirectToAction("Info");

            }
            catch
            {

                ViewBag.Message = $"Course Group with name {courseGroup.Name} is already existed";
                return View("Error");
            }

        }

        [Route("Edit/{id}")]
        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            CourseGroup courseGroup = _iCatalogService.GetCourseGroup(id);
            CourseGroupVM courseGr = _iMapper.Map<CourseGroup, CourseGroupVM>(courseGroup);
            if (courseGroup != null)
            {
                return View(courseGr);
            }

            ViewBag.Message = "Course group was not found";
            return View("Error");


        }

        [Route("Edit/{id}")]
        [HttpPost]
        public IActionResult Edit(CourseGroupVM courseGroup)
        {
            if (!ModelState.IsValid)
            {
                return View(courseGroup);
            }
            CourseGroup courseGr = _iMapper.Map<CourseGroupVM, CourseGroup>(courseGroup);
            try
            {
                _iCatalogService.EditCourseGroup(courseGr);
                return RedirectToAction("Info");
            }
            catch
            {
                ViewBag.Role = "Admin";
                ViewBag.Message = $"Course Group with name {courseGroup.Name} is already existed";
                return View("Error");
            }
        }

        [Route("Delete/{id}")]
        [HttpGet]
        [ActionName("Delete")]
        public IActionResult ConfirmDelete(Guid id)
        {
            {
                CourseGroup courseGroup = _iCatalogService.GetCourseGroup(id);
                CourseGroupVM courseGroupVm = _iMapper.Map<CourseGroup, CourseGroupVM>(courseGroup);

                if (courseGroupVm != null)
                {
                    int countCourses = courseGroup.Course.Count;
                    if (countCourses != 0)
                    {
                        ViewBag.Role = "Admin";
                        ViewBag.Message = $"Course group has dependents and cannot be deleted:  Course: {countCourses}";
                        return View("Error");
                    }
                    return View(courseGroupVm);
                }
            }

            ViewBag.Message = "Course group was not found";
            return View("Error");
        }

        [Route("Delete/{id}")]
        [HttpPost]
        public IActionResult Delete(Guid id)
        {
            {
                CourseGroup courseGroup = _iCatalogService.GetCourseGroup(id);
                if (courseGroup != null && courseGroup.Course.Count == 0)
                {
                    _iCatalogService.DeleteCourseGroup(courseGroup.Id);
                    return RedirectToAction("Info");
                }
            }

            ViewBag.Message = "Course group was not found";
            return View("Error");
        }

        [Route("Error")]
        public IActionResult Error()
        {
            return View("Error");
        }
    }
}