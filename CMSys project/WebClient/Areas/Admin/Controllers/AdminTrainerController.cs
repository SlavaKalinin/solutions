﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BLL.Contract;
using CMSys_project.ViewModels;
using Domain.Entities.UserEntities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CMSys_project.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/Trainer")]
    [Authorize(Roles = "Administrator")]

    public class AdminTrainerController : Controller
    {
        private readonly ICatalogService _iCatalogService;
        private readonly IMapper _iMapper;

        public AdminTrainerController(ICatalogService iCatalogService, IMapper iMapper)
        {
            _iCatalogService = iCatalogService;
            _iMapper = iMapper;
        }

        [Route("Create")]
        public IActionResult Create()
        {
            ViewData.Model = new TrainerVM
            {
                Groups = new SelectList(_iCatalogService.GetAllTrainerGroup(), "Id", "Name"),
                Group = null,
                NonTrainers = new SelectList(_iCatalogService.GetNonTrainers()
                    .Select(u => new { u.Id, FullName = u.Name + " " + u.Surname }), "Id", "FullName")
            };

            return View();
        }

        [Route("Create")]
        [HttpPost]
        public IActionResult Create(TrainerVM trainer)
        {
            if (!ModelState.IsValid)
            {
                return View(trainer);
            }

            Trainer newTrainer = _iMapper.Map<TrainerVM, Trainer>(trainer);

            try
            {
                _iCatalogService.AddTrainer(newTrainer);
                return RedirectToAction("Info");
            }
            catch
            {

                ViewBag.Message = "Something goes wrong!";
                return View("Error");
            }
        }

        [Route("Info")]
        public IActionResult Info()
        {
            IEnumerable<TrainerVM> trainerVMs =
                _iMapper.Map<IEnumerable<Trainer>, IEnumerable<TrainerVM>>(_iCatalogService.GetAllTrainer());

                return View("Info", trainerVMs);
                
        }

        [Route("Edit/{id}")]
        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            TrainerVM trainerVm;
            Trainer trainer;
            try
            {
                trainer = _iCatalogService.GetTrainer(id);
                trainerVm = _iMapper.Map<Trainer, TrainerVM>(trainer);

                trainerVm.NonTrainers = new SelectList(
                    _iCatalogService.GetAllTrainerGroup().Select(t => new { t.Id, t.Name }),
                    "Id", "Name");
            }
            catch (Exception)
            {

                ViewBag.Message = "Trainer was not found!";
                return View("Error");
            }

            ViewData.Model = trainerVm;

            if (trainer != null)
            {
                return View(trainerVm);
            }

            ViewBag.Message = "Trainer was not found!";
            return View("Error");
        }

        [Route("Edit/{id}")]
        [HttpPost]
        public IActionResult Edit(TrainerVM trainerVm)
        {
            if (!ModelState.IsValid)
            {
                return View(trainerVm);
            }

            try
            {
                Trainer trainer = _iMapper.Map<TrainerVM, Trainer>(trainerVm);
                trainer.UserId = trainerVm.Id;
                _iCatalogService.EditTrainer(trainer);
                return RedirectToAction("Info");
            }
            catch
            {

                ViewBag.Message = "Something goes wrong";
                return View("Error");
            }
        }

        [Route("Delete/{id}")]
        [HttpGet]
        [ActionName("Delete")]
        public IActionResult ConfirmDelete(Guid id)
        {
            Trainer trainer = _iCatalogService.GetTrainer(id);
            TrainerVM trainerVm = _iMapper.Map<Trainer, TrainerVM>(trainer);

            if (trainerVm == null)
            {

                ViewBag.Message = "Trainer was not found!";
                return View("Error");
            }

            int countCourses = _iCatalogService.GetAllCourse().SelectMany(course => course.TrainerCourse)
                .Count(trainerCourse => trainerCourse.Trainer.UserId.Equals(trainer.UserId));

            if (countCourses == 0)
            {
                return View(trainerVm);
            }

            ViewBag.Role = "Admin";
            ViewBag.Message = $"Trainer has dependents and cannot be deleted:  Course: {countCourses}";
            return View("Error");
        }

        [Route("Delete/{id}")]
        [HttpPost]
        public IActionResult Delete(Guid id)
        {
            {
                Trainer trainer = _iCatalogService.GetTrainer(id);
                if (trainer != null)
                {
                    _iCatalogService.DeleteTrainer(trainer.UserId);
                    return RedirectToAction("Info");
                }
            }

            ViewBag.Message = "Trainer was not found";
            return View("Error");
        }

        [Route("Profile/{id}")]
        public IActionResult Profile(Guid id)
        {

            Trainer trainer = _iCatalogService.GetTrainer(id);
            ViewData.Model = _iMapper.Map<Trainer, TrainerVM>(trainer);
            if (trainer is null)
            {
                ViewBag.Message = "Trainer was not found";
                return View("Error");
            }

            return View();
        }
    }
}