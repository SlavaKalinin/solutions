﻿using System;
using System.Collections.Generic;
using AutoMapper;
using BLL.Contract;
using CMSys_project.ViewModels;
using Domain.Entities.UserEntities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys_project.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/TrainerGroup")]
    [Authorize(Roles = "Administrator")]
    public class AdminTrainerGroupController : Controller
    {
        private readonly ICatalogService _iCatalogService;
        private readonly IMapper _iMapper;

        public AdminTrainerGroupController(ICatalogService iCatalogService, IMapper iMapper)
        {
            _iCatalogService = iCatalogService;
            _iMapper = iMapper;
        }

        [Route("Info")]
        public IActionResult Info()
        {
            IEnumerable<TrainerGroupVM> trainerGroupVMs =
               _iMapper.Map<IEnumerable<TrainerGroup>, IEnumerable<TrainerGroupVM>>(_iCatalogService.GetAllTrainerGroup());
            return View("Info", trainerGroupVMs);
        }

        [Route("Create")]
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [Route("Create")]
        [HttpPost]
        public IActionResult Create(TrainerGroupVM trainerGroup)
        {
            if (!ModelState.IsValid)
            {
                return View(trainerGroup);
            }
            trainerGroup.Id = Guid.NewGuid();
            TrainerGroup trainerGr = _iMapper.Map<TrainerGroupVM, TrainerGroup>(trainerGroup);
            try
            {
                _iCatalogService.AddTrainerGroup(trainerGr);
                return RedirectToAction("Info");

            }
            catch
            {

                ViewBag.Role = "Admin";
                ViewBag.Message = $"Trainer Group with name {trainerGr.Name} is already existed";
                return View("Error");
            }

        }

        [Route("Edit/{id}")]
        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            TrainerGroup trainerGroup = _iCatalogService.GetTrainerGroup(id);
            TrainerGroupVM trainerGr = _iMapper.Map<TrainerGroup, TrainerGroupVM>(trainerGroup);
            if (trainerGroup != null)
            {
                return View(trainerGr);
            }

            ViewBag.Role = "Admin";
            ViewBag.Message = "Trainer Group was not found";
            return View("Error");
        }

        [Route("Edit/{id}")]
        [HttpPost]
        public IActionResult Edit(TrainerGroupVM trainerGroup)
        {
            if (!ModelState.IsValid)
            {
                return View(trainerGroup);
            }
            TrainerGroup trainerGr = _iMapper.Map<TrainerGroupVM, TrainerGroup>(trainerGroup);
            try
            {
                _iCatalogService.EditTrainerGroup(trainerGr);
                return RedirectToAction("Info");
            }
            catch
            {
                ViewBag.Role = "Admin";
                ViewBag.Message = $"Course Group with name {trainerGr.Name} is already existed";
                return View("Error");
            }
        }

        [Route("Delete/{id}")]
        [HttpGet]
        [ActionName("Delete")]
        public IActionResult ConfirmDelete(Guid id)
        {
            {
                TrainerGroup trainerGroup = _iCatalogService.GetTrainerGroup(id);
                TrainerGroupVM trainerGroupVm = _iMapper.Map<TrainerGroup, TrainerGroupVM>(trainerGroup);

                if (trainerGroupVm != null)
                {
                    int countTrainers = trainerGroup.Trainer.Count;
                    if (countTrainers != 0)
                    {
                        ViewBag.Role = "Admin";
                        ViewBag.Message = $"Course group has dependents and cannot be deleted:  Course: {countTrainers}";
                        return View("Error");
                    }
                    return View(trainerGroupVm);
                }
            }

            ViewBag.Role = "Admin";
            ViewBag.Message = "Trainer group was not found";
            return View("Error");
        }

        [Route("Delete/{id}")]
        [HttpPost]
        public IActionResult Delete(Guid id)
        {

            TrainerGroup trainerGroup = _iCatalogService.GetTrainerGroup(id);
            if (trainerGroup != null && trainerGroup.Trainer.Count == 0)
            {
                _iCatalogService.DeleteTrainerGroup(trainerGroup.Id);
                return RedirectToAction("Info");
            }

            ViewBag.Role = "Admin";
            ViewBag.Message = "Trainer group was not found";
            return View("Error");
        }

        [Route("Error")]
        public IActionResult Error()
        {
            return View("Error");
        }
    }
}