﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CMSys_project.ViewModels
{
    public class TrainerVM : EntityVM<Guid>
    {
        [Required]
        public Guid UserId { get; set; }

        [StringLength(4000, ErrorMessage = "Wrong length!")]
        public string About { get; set; }

        [Required] public Guid GroupId { get; set; }

        public SelectList NonTrainers { get; set; }
        public SelectList Groups { get; set; }
        public TrainerGroupVM Group { get; set; }
        public UserVM User { get; set; }
    }
}