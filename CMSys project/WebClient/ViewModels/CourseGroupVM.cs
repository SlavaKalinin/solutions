﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CMSys_project.ViewModels
{
    // ReSharper disable once InconsistentNaming
    public class CourseGroupVM : EntityVM<Guid>
    {

        [Required]
        [StringLength(32, MinimumLength = 1, ErrorMessage = "Max length is 32")]
        public string Name { get; set; }
    }
}