﻿namespace CMSys_project.ViewModels
{
    public class EntityVM<TIdType> : EntityVM
    {
        public TIdType Id { get; set; }
    }
}