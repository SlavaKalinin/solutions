﻿using System.ComponentModel.DataAnnotations;

namespace CMSys_project.ViewModels
{
    public enum WayPlanningVM
    {
        Demand,
        Schedule
    }
}