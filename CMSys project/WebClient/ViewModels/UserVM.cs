﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CMSys_project.ViewModels
{
    public class UserVM : EntityVM<Guid>
    {
        [EmailAddress]
        [Required]
        [StringLength(128, ErrorMessage = "Wrong length!")]
        public string Email { get; set; }

        [StringLength(256, ErrorMessage = "Wrong length!")]
        public string FullName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime WorkBegin { get; set; }

        [DataType(DataType.Date)]
        public DateTime? WorkEnd { get; set; }

        [StringLength(128, ErrorMessage = "Wrong length!")]
        public string Department { get; set; }

        [StringLength(128, ErrorMessage = "Wrong length!")]
        public string Office { get; set; }

        [StringLength(128, ErrorMessage = "Wrong length!")]
        public string Position { get; set; }

        public List<RoleVM> RoleVm { get; set; }

        public byte[] Photo { get; set; }
    }
}