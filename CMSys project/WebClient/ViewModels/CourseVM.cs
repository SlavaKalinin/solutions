﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Entities.CourseEntities;
using Domain.Entities.UserEntities;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CMSys_project.ViewModels
{
    public class CourseVM : EntityVM<Guid>
    {
        [Required]
        [StringLength(32, MinimumLength = 1, ErrorMessage = "Wrong length!")]
        public string Name { get; set; }

        [Required]
        [EnumDataType(typeof(CourseTypeVM))]
        public CourseTypeVM Type { get; set; }


        [Required]
        [EnumDataType(typeof(WayPlanningVM))]
        public WayPlanningVM Schedule { get; set; }

        [Required]
        public Guid GroupCourseId { get; set; }

        [Required]
        public bool Status { get; set; }

        [StringLength(4000, ErrorMessage = "Wrong length!")]
        public string Description { get; set; }

        public CourseGroupVM CourseGroup { get; set; }
        public Guid SelectedTrainer { get; set; }
        public List<TrainerVM> Trainers { get; set; }
        public SelectList AllTheTrainers { get; set; }
        public SelectList CourseGroups { get; set; }

    }
}