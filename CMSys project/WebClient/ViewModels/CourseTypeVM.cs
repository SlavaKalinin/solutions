﻿namespace CMSys_project.ViewModels
{
    // ReSharper disable once InconsistentNaming
    public enum CourseTypeVM
    {
        Lecture,
        Practical
    }
}