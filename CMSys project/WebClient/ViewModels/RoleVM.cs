﻿using System;

namespace CMSys_project.ViewModels
{
    public class RoleVM
    {
        public int RoleId { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }

    }
}