﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using AutoMapper;
using BLL.Contract;
using CMSys_project.ViewModels;
using Domain.Entities.UserEntities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace CMSys_project.Controllers
{
    [Route("Account")]
    public class AccountController : Controller
    {
        private readonly IMapper _iMapper;
        private readonly IMembershipService _iMembershipService;

        public AccountController(IMembershipService iMembershipService, IMapper iMapper)
        {
            _iMembershipService = iMembershipService;
            _iMapper = iMapper;
        }

        [Route("Login")]
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [Route("Login")]
        [ValidateAntiForgeryToken]
        public IActionResult Login(LoginVM model)
        {

            (User user, string message, bool isExist) = _iMembershipService.Authorize(model.Email, model.Password);

            if (isExist)
            {
                UserVM userVm = _iMapper.Map<User, UserVM>(user);
                userVm.RoleVm = _iMapper.Map<IEnumerable<Role>, IEnumerable<RoleVM>>(user.UserRole.Select(x => x.Role))
                    .ToList();
                Authenticate(userVm);
                if (userVm.RoleVm.Exists(x => x.Name.Equals("Administrator")))
                {
                    return RedirectToAction("Info", "AdminCourse", new {area = "Admin"});
                }

                if (userVm.RoleVm.Exists(x => x.Name.Equals("User")))
                {
                    return RedirectToAction("Info", "UserCourse", new {area = "User"});
                }
            }
            
            return View(model);
        }

        private void Authenticate(UserVM user)
        {
            List<Claim> newClaim = new List<Claim>
            {
                user.RoleVm.Select(x => x.Name).Contains("Administrator")
                    ? new Claim(ClaimsIdentity.DefaultRoleClaimType, "Administrator")
                    : new Claim(ClaimsIdentity.DefaultRoleClaimType, "User"),
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email)
            };

            ClaimsIdentity id = new ClaimsIdentity(newClaim, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public ActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }
    }
}