﻿using System.Collections.Generic;

namespace Domain.Entities.UserEntities
{
    public class Role : Entity
    {
        public Role()
        {
            UserRole = new HashSet<UserRole>();
        }

        public int RoleId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<UserRole> UserRole { get; set; }
    }
}