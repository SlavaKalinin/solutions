﻿using System;

namespace Domain.Entities.UserEntities
{
    public class Trainer : Entity
    {
        public Guid UserId { get; set; }
        public string About { get; set; }
        public Guid GroupId { get; set; }

        public virtual TrainerGroup Group { get; set; }
        public virtual User User { get; set; }
    }
}