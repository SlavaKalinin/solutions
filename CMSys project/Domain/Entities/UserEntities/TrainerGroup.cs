﻿using System;
using System.Collections.Generic;

namespace Domain.Entities.UserEntities
{
    public class TrainerGroup : Entity<Guid>
    {
        public TrainerGroup()
        {
            Trainer = new HashSet<Trainer>();
        }

        public string Name { get; set; }

        public virtual ICollection<Trainer> Trainer { get; set; }
    }
}