﻿using System;
using System.Collections.Generic;

namespace Domain.Entities.UserEntities
{
    public class User : Entity<Guid>
    {
        public User()
        {
            UserRole = new HashSet<UserRole>();
        }

        public string Email { get; set; }
        public string PassHash { get; set; }
        public string PassSalt { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime WorkBegin { get; set; }
        public DateTime? WorkEnd { get; set; }
        public string Department { get; set; }
        public string Office { get; set; }
        public string Position { get; set; }
        public byte[] Photo { get; set; }

        public virtual Trainer Trainer { get; set; }
        public virtual ICollection<UserRole> UserRole { get; set; }
    }
}