﻿using System;
using Domain.Entities.UserEntities;

namespace Domain.Entities.CourseEntities
{
    public class TrainerCourse
    {
        public Guid UserId { get; set; }
        public virtual Trainer Trainer { get; set; }

        public Guid CourseId { get; set; }
        public virtual Course Course { get; set; }
    }
}