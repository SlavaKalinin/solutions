﻿namespace Domain.Entities.CourseEntities
{
    public enum CourseType
    {
        Lecture,
        Practical
    }
}