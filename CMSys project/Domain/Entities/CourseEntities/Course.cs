﻿using System;
using System.Collections.Generic;

namespace Domain.Entities.CourseEntities
{
    public class Course : Entity<Guid>
    {
        public Course()
        {
            TrainerCourse = new HashSet<TrainerCourse>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsNew { get; set; }
        public CourseType Type { get; set; }
        public WayPlanning WayPlanning { get; set; }
        public Guid GroupCourseId { get; set; }

        public virtual CourseGroup CourseGroup { get; set; }
        public virtual ICollection<TrainerCourse> TrainerCourse { get; set; }
    }
}