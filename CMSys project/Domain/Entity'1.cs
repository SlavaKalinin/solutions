﻿namespace Domain
{
    public class Entity<TIdType> : Entity
    {
        public TIdType Id { get; set; }
    }
}