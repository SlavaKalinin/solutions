﻿using System;
using System.Data;
using DAL.Contract;
using DAL.Contract.IRepositories;
using DAL.db.CMSys.Repositories;

namespace DAL.db.CMSys
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CmSysContext _cmSysContext;
        private ICourseGroupRepository _courseGroups;
        private ICourseRepository _courses;
        private IRoleRepository _roles;
        private ITrainerGroupRepository _trainerGroups;
        private ITrainerRepository _trainers;
        private IUserRepository _users;

        public UnitOfWork(CmSysContext cmSysContext)
        {
            _cmSysContext = cmSysContext ?? throw new ArgumentNullException(nameof(cmSysContext));
        }

        public IUserRepository Users => _users = _users ?? new UserRepository(_cmSysContext);
        public ICourseRepository Courses => _courses = _courses ?? new CourseRepository(_cmSysContext);
        public IRoleRepository Roles => _roles = _roles ?? new RoleRepository(_cmSysContext);
        public ITrainerRepository Trainers => _trainers = _trainers ?? new TrainerRepository(_cmSysContext);
        public ITrainerGroupRepository TrainerGroups => _trainerGroups = _trainerGroups ?? new TrainerGroupRepository(_cmSysContext);
        public ICourseGroupRepository CourseGroups => _courseGroups = _courseGroups ?? new CourseGroupRepository(_cmSysContext);

        public void Commit()
        {
            try
            {
                _cmSysContext.SaveChanges();
            }

            catch (Exception e)
            {
                throw new DataException("Commiting error", e);
            }
        }

        public void Dispose()
        {
            _cmSysContext.Dispose();
        }
    }
}