﻿using DAL.db.CMSys.Mappings;
using Domain.Entities.CourseEntities;
using Domain.Entities.UserEntities;
using Microsoft.EntityFrameworkCore;

namespace DAL.db.CMSys
{
    public class CmSysContext : DbContext
    {
        public CmSysContext(DbContextOptions<CmSysContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public virtual DbSet<Course> Course { get; set; }
        public virtual DbSet<CourseGroup> CourseGroup { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Trainer> Trainer { get; set; }
        public virtual DbSet<TrainerCourse> TrainerCourse { get; set; }
        public virtual DbSet<TrainerGroup> TrainerGroup { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CourseMap());

            modelBuilder.ApplyConfiguration(new CourseGroupMap());

            modelBuilder.ApplyConfiguration(new RoleMap());

            modelBuilder.ApplyConfiguration(new TrainerMap());

            modelBuilder.ApplyConfiguration(new TrainerCourseMap());

            modelBuilder.ApplyConfiguration(new TrainerGroupMap());

            modelBuilder.ApplyConfiguration(new UserMap());

            modelBuilder.ApplyConfiguration(new UserRoleMap());
        }
    }
}