﻿using Domain.Entities.CourseEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.db.CMSys.Mappings
{
    public class CourseMap : IEntityTypeConfiguration<Course>
    {
        public void Configure(EntityTypeBuilder<Course> builder)
        {
            builder.Property(e => e.Id)
                .HasColumnName("CourseId")
                .ValueGeneratedNever();

            builder.Property(e => e.Description)
                .HasMaxLength(4000);

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(64);

            builder.HasOne(d => d.CourseGroup)
                .WithMany(p => p.Course)
                .HasForeignKey(d => d.GroupCourseId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Course_GroupCourseId");
        }
    }
}