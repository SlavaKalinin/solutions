﻿using Domain.Entities.UserEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.db.CMSys.Mappings
{
    public class TrainerGroupMap : IEntityTypeConfiguration<TrainerGroup>
    {
        public void Configure(EntityTypeBuilder<TrainerGroup> builder)
        {
            builder.HasIndex(e => e.Name)
                .HasName("AK_TrainerGroup_Name")
                .IsUnique();

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("TrainerGroupId");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(32);
        }
    }
}