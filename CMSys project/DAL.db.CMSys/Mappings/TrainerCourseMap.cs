﻿using Domain.Entities.CourseEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.db.CMSys.Mappings
{
    public class TrainerCourseMap : IEntityTypeConfiguration<TrainerCourse>
    {
        public void Configure(EntityTypeBuilder<TrainerCourse> builder)
        {
            builder.HasKey(e => new {TrainerId = e.UserId, e.CourseId})
                .HasName("PK_TrainerCourse_xref_UserId_CourseId");

            builder.ToTable("TrainerCourse_xref");

            builder.HasOne(d => d.Course)
                .WithMany(p => p.TrainerCourse)
                .HasForeignKey(d => d.CourseId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TrainerCourse_xref_Course_CourseId");

            //builder.HasOne(d => d.Trainer)
            //    .WithMany(p => p.TrainerCourse)
            //    .HasForeignKey(d => d.UserId)
            //    .OnDelete(DeleteBehavior.ClientSetNull)
            //    .HasConstraintName("FK_TrainerCourse_xref_Trainer_UserId");
        }
    }
}