﻿using Domain.Entities.UserEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.db.CMSys.Mappings
{
    public class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
                .HasIndex(e => e.Email)
                .HasName("AK_User_Email")
                .IsUnique();

            builder.Property(e => e.Id)
                .HasColumnName("userId")
                .ValueGeneratedNever();

            builder.Property(e => e.Department)
                .HasMaxLength(128);

            builder.Property(e => e.Email)
                .IsRequired()
                .HasMaxLength(128);

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(128);

            builder.Property(e => e.Office)
                .HasMaxLength(128);

            builder.Property(e => e.PassHash)
                .IsRequired()
                .HasMaxLength(128);

            builder.Property(e => e.PassSalt)
                .IsRequired()
                .HasMaxLength(128);

            builder.Property(e => e.Position)
                .HasMaxLength(128);

            builder.Property(e => e.Surname)
                .IsRequired()
                .HasMaxLength(128);

            builder.Property(e => e.WorkBegin)
                .HasColumnType("date");

            builder.Property(e => e.WorkEnd)
                .HasColumnType("date");
        }
    }
}