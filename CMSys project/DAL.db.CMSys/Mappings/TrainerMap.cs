﻿using Domain.Entities.UserEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.db.CMSys.Mappings
{
    public class TrainerMap : IEntityTypeConfiguration<Trainer>
    {
        public void Configure(EntityTypeBuilder<Trainer> builder)
        {
            builder.HasKey(e => e.UserId)
                .HasName("PK_Trainer_UserId");

            builder.Property(e => e.UserId)
                .ValueGeneratedNever();

            builder.Property(e => e.About)
                .HasMaxLength(4000);

            builder.HasOne(d => d.Group)
                .WithMany(p => p.Trainer)
                .HasForeignKey(d => d.GroupId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(d => d.User)
                .WithOne(p => p.Trainer)
                .HasForeignKey<Trainer>(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}