﻿using System;
using DAL.Contract.IRepositories;
using Domain.Entities.UserEntities;

namespace DAL.db.CMSys.Repositories
{
    public class TrainerRepository : Repository<Trainer, Guid>, ITrainerRepository
    {
        private CmSysContext _cmSysContext;

        public TrainerRepository(CmSysContext cmSysContext) : base(cmSysContext)
        {
            _cmSysContext = cmSysContext;
        }
    }
}