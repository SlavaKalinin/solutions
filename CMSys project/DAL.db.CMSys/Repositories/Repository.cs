﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using DAL.Contract.IRepositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.db.CMSys.Repositories
{
    public class Repository<T, TypeId> : IRepository<T, TypeId> where T : Entity

    {
        private readonly CmSysContext _cmSysContext;
        private readonly DbSet<T> _dbSet;

        public Repository(CmSysContext cmSysContext)
        {
            _cmSysContext = cmSysContext;
            _dbSet = _cmSysContext.Set<T>();
        }

        public IEnumerable<T> GetAll()
        {
            return _dbSet.ToList();
        }

        public T Get(TypeId id)
        {
            return _dbSet.Find(id);
        }

        public IQueryable<T> Get(Expression<Func<T, bool>> predicate)
        {
            if (predicate is null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            return _dbSet.Where(predicate);
        }

        public void Add(T entity)
        {
            try
            {
                _dbSet.Add(entity);
            }
            catch (Exception e)
            {
                throw new DataException(nameof(entity), e);
            }
        }

        public void Delete(T entity)
        {
            try
            {
                _dbSet.Remove(entity);
            }
            catch (Exception e)
            {
                throw new DataException(nameof(entity), e);
            }
        }

        public void Delete(TypeId id)
        {
            try
            {
                _dbSet.Remove(Get(id));
            }
            catch (Exception e)
            {
                throw new DataException(nameof(id), e);
            }
        }

    }
}