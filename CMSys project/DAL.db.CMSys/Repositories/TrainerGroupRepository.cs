﻿using System;
using DAL.Contract.IRepositories;
using Domain.Entities.UserEntities;

namespace DAL.db.CMSys.Repositories
{
    public class TrainerGroupRepository : Repository<TrainerGroup, Guid>, ITrainerGroupRepository
    {
        private CmSysContext _cmSysContext;

        public TrainerGroupRepository(CmSysContext cmSysContext) : base(cmSysContext)
        {
            _cmSysContext = cmSysContext;
        }
    }
}