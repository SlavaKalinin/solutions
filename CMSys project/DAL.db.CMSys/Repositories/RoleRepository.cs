﻿using DAL.Contract.IRepositories;
using Domain.Entities.UserEntities;

namespace DAL.db.CMSys.Repositories
{
    public class RoleRepository : Repository<Role, int>, IRoleRepository
    {
        private CmSysContext _cmSysContext;

        public RoleRepository(CmSysContext cmSysContext) : base(cmSysContext)
        {
            _cmSysContext = cmSysContext;
        }
    }
}