﻿using System;
using DAL.Contract.IRepositories;
using Domain.Entities.UserEntities;

namespace DAL.db.CMSys.Repositories
{
    public class UserRepository : Repository<User, Guid>, IUserRepository
    {
        private readonly CmSysContext _cmSysContext;

        public UserRepository(CmSysContext cmSysContext) : base(cmSysContext)
        {
            _cmSysContext = cmSysContext;
        }
    }
}