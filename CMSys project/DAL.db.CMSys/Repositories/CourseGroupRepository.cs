﻿using System;
using DAL.Contract.IRepositories;
using Domain.Entities.CourseEntities;

namespace DAL.db.CMSys.Repositories
{
    public class CourseGroupRepository : Repository<CourseGroup, Guid>, ICourseGroupRepository
    {
        private CmSysContext _cmSysContext;

        public CourseGroupRepository(CmSysContext cmSysContext) : base(cmSysContext)
        {
            _cmSysContext = cmSysContext;
        }
    }
}