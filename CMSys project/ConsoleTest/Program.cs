﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using DAL.db.CMSys;
using Domain.Entities.UserEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace ConsoleTest
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var hash = SecurePasswordHasher.Hash("mypassword");
            // Verify
         //   Console.WriteLine(GetPasswordSalt());
            //   Console.WriteLine(GetPasswordHash("12345", "rxRaXafnh8BdDjGn2"));
               Console.WriteLine(EqualPasswords("ElIxrl57Uaxxtzf9R9y1HNCp+X9EWdikBp7EmMplvIs=", "12345", "rxRaXafnh8BdDjGn2"));
        }

        private static DbContextOptions<CmSysContext> GetBuilder(string fileName)
        {
            ConfigurationBuilder builder = new ConfigurationBuilder();
            IConfiguration config = builder.SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(fileName).Build();
            string connectStr = config.GetConnectionString("DefaultConnection");
            return new DbContextOptionsBuilder<CmSysContext>().UseLazyLoadingProxies().UseSqlServer(connectStr).Options;
        }
        public static bool EqualPasswords(string hash, string password, string salt)
        {
          //  Expect.ArgumentNotNull(user, nameof(user));
           // Expect.ArgumentNotNull(password, nameof(password));

            return hash == GetPasswordHash(password, salt);
        }

        private static string GetPasswordSalt()
        {
            var rng = RandomNumberGenerator.Create();
            var bytes = new byte[128];
            rng.GetBytes(bytes);

            return Convert.ToBase64String(bytes);
        }

        private static string GetPasswordHash(string password, string salt) =>
            Convert.ToBase64String(new SHA256Managed()
                .ComputeHash(Encoding.UTF8.GetBytes(password + salt)));
    }
}
