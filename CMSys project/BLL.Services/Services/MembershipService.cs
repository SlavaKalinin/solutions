﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using BLL.Contract;
using DAL.Contract;
using Domain.Entities.UserEntities;

namespace BLL.Services
{
    public class MemberShipService : IMembershipService
    {
        private readonly IUnitOfWork _unitOfWork;

        public MemberShipService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException();
        }

        #region User

        public void EditUser(User user, List<UserRole> userRoles)
        {
            if (user is null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            User newUser = _unitOfWork.Users.Get(user.Id);
            if (newUser is null)
            {
                throw new ArgumentNullException(nameof(newUser));
            }

            foreach (UserRole userRole in userRoles)
            {
                newUser.UserRole.Remove(userRole);
            }

            newUser.UserRole = userRoles;
            newUser.WorkEnd = user.WorkEnd;
            newUser.Photo = user.Photo;
            _unitOfWork.Commit();
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _unitOfWork.Users.GetAll();
        }

        public User GetUser(Guid id)
        {
            return _unitOfWork.Users.Get(id);
        }

        public IEnumerable<Role> GetAllRoles()
        {
            return _unitOfWork.Roles.GetAll();
        }

        public (User, string, bool) Authorize(string email, string password)
        {
            User user = _unitOfWork.Users.Get(x => x.Email.Equals(email)).First();
            if (user is null)
            {
                return (null, "No user with such email", false);
            }

            if (EqualPasswords(user, password))
            {
                return (user, null, true);
            }

            return (null, "Password is incorrect", false);

        }

        public Role GetRole(int id)
        {
            return _unitOfWork.Roles.Get(id);
        }

        private bool EqualPasswords(User user, string password)
        {
            return user.PassHash == GetPasswordHash(password, user.PassSalt);
        }

        private string GetPasswordHash(string password, string salt)
        {
            return Convert.ToBase64String(new SHA256Managed()
                .ComputeHash(Encoding.UTF8.GetBytes(password + salt)));
        }

        #endregion
    }
}