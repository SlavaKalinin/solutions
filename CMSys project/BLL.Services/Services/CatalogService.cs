﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Contract;
using DAL.Contract;
using Domain.Entities.CourseEntities;
using Domain.Entities.UserEntities;

namespace BLL.Services
{
    public class CatalogService : ICatalogService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CatalogService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }

        #region CourseGroup

        public void AddCourseGroup(CourseGroup courseGroup)
        {
            if (courseGroup is null)
            {
                throw new ArgumentNullException(nameof(courseGroup));
            }

            _unitOfWork.CourseGroups.Add(courseGroup);
            _unitOfWork.Commit();
        }

        public void DeleteCourseGroup(Guid id)
        {
            _unitOfWork.CourseGroups.Delete(id);
            _unitOfWork.Commit();
        }

        public void EditCourseGroup(CourseGroup courseGroup)
        {
            if (courseGroup is null)
            {
                throw new ArgumentNullException(nameof(courseGroup));
            }

            CourseGroup courseGr = _unitOfWork.CourseGroups.Get(courseGroup.Id);
            if (courseGr is null)
            {
                throw new ArgumentNullException(nameof(courseGr));
            }
            courseGr.Name = courseGroup.Name;
            _unitOfWork.Commit();
        }

        public IEnumerable<CourseGroup> GetAllCourseGroup()
        {
            return _unitOfWork.CourseGroups.GetAll();
        }

        public CourseGroup GetCourseGroup(Guid id)
        {
            return _unitOfWork.CourseGroups.Get(id);
        }

        #endregion

        #region TrainerGroup

        public void AddTrainerGroup(TrainerGroup trainerGroup)
        {
            if (trainerGroup is null)
            {
                throw new ArgumentNullException();
            }

            _unitOfWork.TrainerGroups.Add(trainerGroup);
            _unitOfWork.Commit();
        }

        public void DeleteTrainerGroup(Guid id)
        {
            _unitOfWork.TrainerGroups.Delete(id);
            _unitOfWork.Commit();
        }

        public void EditTrainerGroup(TrainerGroup trainerGroup)
        {
            if (trainerGroup is null)
            {
                throw new ArgumentNullException();
            }

            TrainerGroup trainerGr = _unitOfWork.TrainerGroups.Get(trainerGroup.Id);
            if (trainerGr is null)
            {
                throw new ArgumentNullException(nameof(trainerGr));
            }
            trainerGr.Name = trainerGroup.Name;
            _unitOfWork.Commit();
        }

        public IEnumerable<TrainerGroup> GetAllTrainerGroup()
        {
            return _unitOfWork.TrainerGroups.GetAll();
        }

        public TrainerGroup GetTrainerGroup(Guid id)
        {
            return _unitOfWork.TrainerGroups.Get(id);
        }

        #endregion

        #region Trainer

        public void AddTrainer(Trainer trainer)
        {
            if (trainer is null)
            {
                throw new ArgumentNullException();
            }

            _unitOfWork.Trainers.Add(trainer);
            _unitOfWork.Commit();
        }

        public void DeleteTrainer(Guid id)
        {
            _unitOfWork.Trainers.Delete(id);
            _unitOfWork.Commit();
        }

        public void EditTrainer(Trainer trainer)
        {
            if (trainer is null)
            {
                throw new ArgumentNullException();
            }

            Trainer newTrainer = _unitOfWork.Trainers.Get(trainer.UserId);
            if (newTrainer is null)
            {
                throw new ArgumentNullException(nameof(newTrainer));
            }
            newTrainer.GroupId = trainer.GroupId;
            newTrainer.About = trainer.About;
            _unitOfWork.Commit();
        }

        public IEnumerable<Trainer> GetAllTrainer()
        {
            return _unitOfWork.Trainers.GetAll();
        }

        public Trainer GetTrainer(Guid id)
        {
            return _unitOfWork.Trainers.Get(id);
        }

        public IEnumerable<User> GetNonTrainers()
        {
            return _unitOfWork.Users.Get(z => z.Trainer.Equals(null));
        }

        #endregion

        #region Course

        public void AddCourse(Course course)
        {
            if (course is null)
            {
                throw new ArgumentNullException();
            }

            _unitOfWork.Courses.Add(course);
            _unitOfWork.Commit();
        }

        public void DeleteCourse(Guid id)
        {
            _unitOfWork.Courses.Delete(id);
            _unitOfWork.Commit();
        }

        public void EditCourse(Course course)
        {
            if (course is null)
            {
                throw new ArgumentNullException();
            }

            Course newCourse = _unitOfWork.Courses.Get(course.Id);
            if (newCourse is null)
            {
                throw new ArgumentNullException();
            }
            newCourse.GroupCourseId = course.GroupCourseId;
            newCourse.WayPlanning = course.WayPlanning;
            newCourse.Name = course.Name;
            newCourse.IsNew = course.IsNew;
            newCourse.Type = course.Type;
            newCourse.Description = course.Description;
            _unitOfWork.Commit();
        }

        public IEnumerable<Course> GetAllCourse()
        {
            return _unitOfWork.Courses.GetAll();
        }

        public Course GetCourse(Guid id)
        {
            return _unitOfWork.Courses.Get(id);
        }

        public IEnumerable<Trainer> GetCourseTrainers(Course course)
        {
            if (course is null)
            {
                throw new ArgumentNullException();
            }
            return course.TrainerCourse.Select(x => x.Trainer);
        }

        public void DeleteCourseTrainer(Course course, Trainer trainer)
        {
            if (course is null)
            {
                throw new ArgumentNullException(nameof(course));
            }

            if (trainer is null)
            {
                throw new ArgumentNullException(nameof(trainer));
            }
            Course newCourse = _unitOfWork.Courses.Get(course.Id);
            if (newCourse is null)
            {
                throw new ArgumentNullException(nameof(newCourse));
            }
            newCourse.TrainerCourse.Remove(newCourse.TrainerCourse.First(x => x.UserId.Equals(trainer.UserId)));
            _unitOfWork.Commit();
        }

        public void AddTrainerCourse(Course course, Trainer trainer)
        {
            if (course is null)
            {
                throw new ArgumentNullException(nameof(course));
            }

            if (trainer is null)
            {
                throw new ArgumentNullException(nameof(trainer));
            }
            Course newCourse = _unitOfWork.Courses.Get(course.Id);
            if (newCourse is null)
            {
                throw new ArgumentNullException(nameof(newCourse));
            }
            newCourse.TrainerCourse.Add(new TrainerCourse
                {Course = newCourse, CourseId = newCourse.Id, Trainer = trainer, UserId = trainer.UserId});
            _unitOfWork.Commit();
        }

        #endregion

     
    }
}