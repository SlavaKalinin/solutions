﻿using System;
using System.Collections.Generic;
using Domain.Entities.UserEntities;

namespace BLL.Contract
{
    public interface IMembershipService
    {
        #region User

        void EditUser(User user, List<UserRole> userRole);
        IEnumerable<User> GetAllUsers();
        User GetUser(Guid id);
        IEnumerable<Role> GetAllRoles();
      //  bool EqualPasswords(User user, string password);
        (User, string, bool) Authorize(string email, string password);
        Role GetRole(int id);

        #endregion
    }
}