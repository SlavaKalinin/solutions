﻿using System;
using System.Collections.Generic;
using Domain.Entities.CourseEntities;
using Domain.Entities.UserEntities;

namespace BLL.Contract
{
    public interface ICatalogService
    {
        #region CourseGroup

        void AddCourseGroup(CourseGroup courseGroup);
        void DeleteCourseGroup(Guid id);
        void EditCourseGroup(CourseGroup courseGroup);
        IEnumerable<CourseGroup> GetAllCourseGroup();
        CourseGroup GetCourseGroup(Guid id);

        #endregion

        #region TrainerGroup

        void AddTrainerGroup(TrainerGroup trainerGroup);
        void DeleteTrainerGroup(Guid id);
        void EditTrainerGroup(TrainerGroup trainerGroup);
        IEnumerable<TrainerGroup> GetAllTrainerGroup();
        TrainerGroup GetTrainerGroup(Guid id);

        #endregion

        #region Trainer

        void AddTrainer(Trainer trainer);
        void DeleteTrainer(Guid id);
        void EditTrainer(Trainer trainer);
        IEnumerable<Trainer> GetAllTrainer();
        Trainer GetTrainer(Guid id);
        IEnumerable<User> GetNonTrainers();

        #endregion

        #region Course

        void AddCourse(Course course);
        void DeleteCourse(Guid id);
        void EditCourse(Course course);
        IEnumerable<Course> GetAllCourse();
        Course GetCourse(Guid id);
        IEnumerable<Trainer> GetCourseTrainers(Course course);
        void DeleteCourseTrainer(Course course, Trainer trainer);
        void AddTrainerCourse(Course course, Trainer trainer);

        #endregion

    }
}