﻿using System;
using DAL.Contract.IRepositories;

namespace DAL.Contract
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository Users { get; }
        ICourseGroupRepository CourseGroups { get; }
        ICourseRepository Courses { get; }
        IRoleRepository Roles { get; }
        ITrainerGroupRepository TrainerGroups { get; }
        ITrainerRepository Trainers { get; }
        void Commit();
    }
}