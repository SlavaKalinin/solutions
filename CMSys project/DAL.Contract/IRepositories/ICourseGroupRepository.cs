﻿using System;
using Domain.Entities.CourseEntities;

namespace DAL.Contract.IRepositories
{
    public interface ICourseGroupRepository : IRepository<CourseGroup, Guid>
    {
    }
}