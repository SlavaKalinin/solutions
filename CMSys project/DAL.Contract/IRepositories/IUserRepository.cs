﻿using System;
using Domain.Entities.UserEntities;

namespace DAL.Contract.IRepositories
{
    public interface IUserRepository : IRepository<User, Guid>
    {
    }
}