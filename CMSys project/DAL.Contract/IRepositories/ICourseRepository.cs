﻿using System;
using Domain.Entities.CourseEntities;

namespace DAL.Contract.IRepositories
{
    public interface ICourseRepository : IRepository<Course, Guid>
    {
    }
}