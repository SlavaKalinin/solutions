﻿using System;
using Domain.Entities.UserEntities;

namespace DAL.Contract.IRepositories
{
    public interface ITrainerGroupRepository : IRepository<TrainerGroup, Guid>
    {
    }
}