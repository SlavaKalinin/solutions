﻿using System;
using Domain.Entities.UserEntities;

namespace DAL.Contract.IRepositories
{
    public interface ITrainerRepository : IRepository<Trainer, Guid>
    {
    }
}