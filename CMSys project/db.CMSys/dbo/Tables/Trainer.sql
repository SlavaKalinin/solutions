﻿CREATE TABLE [dbo].[Trainer](
    [UserId]      UNIQUEIDENTIFIER       NOT NULL, 
    [About]       NVARCHAR(4000),
    [GroupId]     UNIQUEIDENTIFIER       NOT NULL

    CONSTRAINT  [PK_Trainer_UserId]           PRIMARY KEY ([UserId])

    CONSTRAINT  [FK_Trainer_User_UserId]              FOREIGN KEY ([UserId])      REFERENCES  [dbo].[User]([UserId])                  ON DELETE CASCADE
    CONSTRAINT  [FK_Trainer_TrainerGroup_GroupId]     FOREIGN KEY ([GroupId])     REFERENCES  [dbo].[TrainerGroup]([TrainerGroupId])  ON DELETE CASCADE

)
GO