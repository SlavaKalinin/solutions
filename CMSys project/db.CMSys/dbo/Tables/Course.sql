﻿CREATE TABLE [dbo].[Course](
	[CourseId]          UNIQUEIDENTIFIER        NOT NULL,
	[Name]              NVARCHAR(64)            NOT NULL,
	[Description]       NVARCHAR(4000),
	[IsNew]             BIT                     NOT NULL      DEFAULT 0,
	[Type]              INT                     NOT NULL,
	[WayPlanning]       INT                     NOT NULL,
	[GroupCourseId]     UNIQUEIDENTIFIER        NOT NULL
	
	CONSTRAINT  [PK_Course_CourseId]        PRIMARY KEY ([CourseId])

	CONSTRAINT  [FK_Course_GroupCourseId]   FOREIGN KEY ([GroupCourseId])     REFERENCES  [dbo].[CourseGroup]([CourseGroupId])  ON DELETE CASCADE
)
GO