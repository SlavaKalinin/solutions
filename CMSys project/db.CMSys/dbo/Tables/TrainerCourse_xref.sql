﻿CREATE TABLE [dbo].[TrainerCourse_xref](
    [UserId]       UNIQUEIDENTIFIER        NOT NULL, 
    [CourseId]     UNIQUEIDENTIFIER        NOT NULL

    CONSTRAINT  [PK_TrainerCourse_xref_UserId_CourseId]   PRIMARY KEY 
    (
        [UserId],
        [CourseId]
    )

    CONSTRAINT  [FK_TrainerCourse_xref_Trainer_UserId]    FOREIGN KEY ([UserId])       REFERENCES [dbo].[Trainer]([UserId])   ON DELETE CASCADE
    CONSTRAINT  [FK_TrainerCourse_xref_Course_CourseId]   FOREIGN KEY ([CourseId])     REFERENCES [dbo].[Course]([CourseId])  ON DELETE CASCADE
)
GO