﻿CREATE TABLE [dbo].[UserRole_xref](
    [UserId]     UNIQUEIDENTIFIER        NOT NULL, 
    [RoleId]     INT                     NOT NULL

    CONSTRAINT  [PK_UserRole_xref_UserId_RoleId]   PRIMARY KEY 
    (
        [UserId],
        [RoleId]
    )

    CONSTRAINT  [FK_UserRole_xref_User_UserId]   FOREIGN KEY ([UserId])     REFERENCES [dbo].[User]([UserId])  ON DELETE CASCADE
    CONSTRAINT  [FK_UserRole_xref_Role_RoleId]   FOREIGN KEY ([RoleId])     REFERENCES [dbo].[Role]([RoleId])  ON DELETE CASCADE
)
GO