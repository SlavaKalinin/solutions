﻿CREATE TABLE [dbo].[TrainerGroup](
    [TrainerGroupId]     UNIQUEIDENTIFIER        NOT NULL, 
    [Name]               NVARCHAR(32)            NOT NULL

    CONSTRAINT  [PK_TrainerGroup_TrainerGroupId]   PRIMARY KEY ([TrainerGroupId])

    CONSTRAINT  [AK_TrainerGroup_Name]             UNIQUE        ([Name])
)
GO