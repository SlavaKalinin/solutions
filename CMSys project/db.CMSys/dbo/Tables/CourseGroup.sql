﻿CREATE TABLE [dbo].[CourseGroup](
    [CourseGroupId]     UNIQUEIDENTIFIER        NOT NULL,
    [Name]              NVARCHAR(32)            NOT NULL

    CONSTRAINT  [PK_CourseGroup_CourseGroupId]   PRIMARY KEY ([CourseGroupId])
	CONSTRAINT  [AK_CourseGroup_Name]            UNIQUE      ([Name])
)
GO