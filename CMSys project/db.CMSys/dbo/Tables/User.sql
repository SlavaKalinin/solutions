﻿CREATE TABLE dbo.[User](
    [UserId]         UNIQUEIDENTIFIER        NOT NULL, 
    [Email]          NVARCHAR(128)           NOT NULL,
    [PassHash]       NVARCHAR(128)           NOT NULL,
    [PassSalt]       NVARCHAR(128)           NOT NULL,
    [Name]           NVARCHAR(128)           NOT NULL,
    [Surname]        NVARCHAR(128)           NOT NULL,
    [WorkBegin]      DATE                    NOT NULL,
    [WorkEnd]        DATE,
    [Department]     NVARCHAR(128),
    [Office]         NVARCHAR(128),
    [Position]       NVARCHAR(128),
    [Photo]          VARBINARY(MAX)

    CONSTRAINT  [PK_User_UserId]       PRIMARY KEY ([UserId])

    CONSTRAINT  [AK_User_Email]        UNIQUE      ([Email])
)
GO