﻿--Role
INSERT INTO [dbo].[Role] ([Name])
VALUES  ('Administrator'),
		('User')
GO
--User
INSERT INTO [dbo].[User] ([UserId], [Email], [PassHash], [PassSalt], [Name], [Surname], [WorkBegin], [Photo],
						 [Department], [Office], [Position])
VALUES
	(	'C8BCE01D-53AB-4E8F-8CFA-49A4F61500CB',
		'someemail@gmail.com',
		'OSk2GHvGxV025rpUqC2TBj6dJiPbLJJnOJGnDWiW9j8=',
		'62dRsOrrlZ5ubj',
		'Anton', 'Krulov', '2015-04-05', CAST('ÿØÿà' AS VARBINARY),
		'Engineer', 'Chapaeva, 5', '.NET'),

	(	'C8BCE01D-53AB-4E8F-8CFA-49A4F61577CB',
		'anotheremail@gmail.com',
		'$2y$10$0Y5Yzt6sRRpIIXLGCXFVuu7IQohjgh5JOODPBa5Cvmo7MgC',
		'Y3hGRls-ANHuA_S',
		'Uri', 'Belov', '2012-04-05', CAST('�က䙊䙉Āā怀怀' AS VARBINARY),
		'Engineer', 'Chapaeva, 5', '.NET') ,

	(	'C8BCE01D-53AB-4E8F-8CFA-49A4F62500CB',
		'onemoreemail@gmail.com',
		'$2y$10$0Y5Yzt6sRyfjhfjXFVuu7IQohjgh5JOODPBa5Cvmo7MgC',
		'Y3hGhjhjjs-ANHuA_S', 
		'Max', 'Skripnik', '2017-04-20', CAST('ÿØÿà' AS VARBINARY),
		'Driver', 'Zetkin, 51', NULL),
	
	(	'C8BCE01D-53AB-4E8F-8CFA-49A5C61000CB',
		'nothisemail@gmail.com',
		'$2y$10$0jksdksRyfjhfjXFVuu7IQohjgh5JOODPBa5Cvmo7MgC',
		'Y3hGhjhjjs-ANknuA_S',
		'Anrdey', 'Pugovkin', '2008-10-13', CAST('ÿØÿà' AS VARBINARY),
		'Driver', 'Chapaeva, 5' , 'Java'),
		
	(	'C8BCE01D-52AB-2E8F-7CFA-46A5C61000CB', 
		'notmyemail@gmail.com',
		'ElIxrl57Uaxxtzf9R9y1HNCp+X9EWdikBp7EmMplvIs=',
		'rxRaXafnh8BdDjGn2', 
		'Anna', 'Merkush', '2019-05-10', CAST('ÿØÿà' AS VARBINARY),
		'Engineer', 'Chapaeva, 5', 'BI') 
GO
--UserRole_xref
DELETE FROM [dbo].UserRole_xref
INSERT INTO [dbo].[UserRole_xref] ([UserId], [RoleId])
	(SELECT   'C8BCE01D-53AB-4E8F-8CFA-49A4F61500CB', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   'C8BCE01D-53AB-4E8F-8CFA-49A4F61500CB', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'Administrator')
		UNION
	(SELECT   'C8BCE01D-53AB-4E8F-8CFA-49A4F61577CB', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   'C8BCE01D-53AB-4E8F-8CFA-49A4F62500CB', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   'C8BCE01D-53AB-4E8F-8CFA-49A4F62500CB', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'Administrator')
		UNION
	(SELECT   'C8BCE01D-53AB-4E8F-8CFA-49A5C61000CB', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   'C8BCE01D-52AB-2E8F-7CFA-46A5C61000CB', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
GO
--GroupTrainers
INSERT INTO [dbo].[TrainerGroup] ([TrainerGroupId], [Name])
VALUES  ('7F2898AC-5644-4672-A3E0-CC8364571D63', 'Java'),
		('131C3018-48D9-4459-8EEC-C516D13CE504', 'DB')
GO
--Trainer
INSERT INTO [dbo].[Trainer] ([UserId], [About], [GroupId])
VALUES  ('C8BCE01D-53AB-4E8F-8CFA-49A4F61500CB', 'Some info about me', '7F2898AC-5644-4672-A3E0-CC8364571D63'),
		('C8BCE01D-53AB-4E8F-8CFA-49A4F62500CB', NULL, '131C3018-48D9-4459-8EEC-C516D13CE504')
GO
--GroupCourses
INSERT INTO [dbo].[CourseGroup] ([CourseGroupId], [Name])
VALUES  ('70D291B3-5665-47E7-9ADF-59C3BA5D5230', 'Java'),
		('DFB68B68-7687-4413-B408-8CBEE50EAA26', 'DB')
GO
--Course
INSERT INTO [dbo].[Course] ([CourseId], [Description], [GroupCourseId], [IsNew], [Name], [WayPlanning], [Type])
VALUES  ('DB01395C-2915-43A6-B9F1-ADA582F2155A','This course is for people, who wants to learn sql', '70D291B3-5665-47E7-9ADF-59C3BA5D5230',
		 0, 'Sql for dev', 0, 1),
		('01F4F097-3245-4869-A378-297B84AD60CA', 'This course is for people, who wants to learn java', 'DFB68B68-7687-4413-B408-8CBEE50EAA26',
		 1, 'Java for begin', 1, 0)
GO
--TrainerCourse_xref
INSERT INTO [dbo].[TrainerCourse_xref] ([CourseId], [UserId])
VALUES  ('DB01395C-2915-43A6-B9F1-ADA582F2155A', 'C8BCE01D-53AB-4E8F-8CFA-49A4F61500CB'),
		('01F4F097-3245-4869-A378-297B84AD60CA', 'C8BCE01D-53AB-4E8F-8CFA-49A4F62500CB')
GO