﻿using System;

namespace NET01._1
{
    public static class IdGenerator
    {
        public static void IdGenerate(this Entity entity)
        {
            entity.Id = Guid.NewGuid();
        }
    }
}

