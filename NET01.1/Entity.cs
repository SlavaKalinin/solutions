﻿using System;

namespace NET01._1
{
    public abstract class Entity
    {
        private string _description;

        protected Entity()
        {
        }

        protected Entity(string description)
        {
            CheckForLong(description);
            _description = description;
        }

        protected Entity(string description, Guid guid)
        {
            CheckForLong(description);
            _description = description;
            Id = guid;
        }

        public Guid Id { get; set; }

        public string Description
        {
            get => _description;
            set
            {
                CheckForLong(value);
                _description = value;
            }
        }

        public override string ToString()
        {
            return _description;
        }

        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (this == obj) return true;
            if (obj.GetType() != GetType()) return false;
            var another = (Entity) obj;
            return another.Id == Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        private void CheckForLong(string value)
        {
            if (value?.Length > 256) throw new ArgumentException("Description is too long, must be less 257 symbols");
        }
    }
}