﻿namespace NET01._1
{
    public interface IVersionable
    {
        byte[] GetVersion();
        void SetVersion(byte[] bytes);
    }
}
