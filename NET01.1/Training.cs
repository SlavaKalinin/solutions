﻿using System;

namespace NET01._1
{
    public class Training : Entity, ICloneable, IVersionable
    {
        private readonly byte[] _version = new byte[8];

        private Material[] _materials = new Material[0];

        public Material this[int index] => _materials[index];

        public object Clone()
        {
            var cloneTraining = new Training();
            foreach (var lesson in _materials) cloneTraining.AddMaterial(lesson.Clone());

            return cloneTraining;
        }

        public byte[] GetVersion()
        {
            var version = new byte[8];
            _version.CopyTo(version, 0);
            return version;
        }

        public void SetVersion(byte[] version)
        {
            if (version == null) throw new ArgumentException("Array is null!");

            if (version.Length != _version.Length) throw new ArgumentException("Length of version != 8");

            version.CopyTo(_version, 0);
        }

        public void AddMaterial(Material material)
        {
            if (material == null) throw new ArgumentNullException(nameof(material));

            Array.Resize(ref _materials, _materials.Length + 1);
            _materials[_materials.Length - 1] = material;
        }

        public LessonType TypeOfLesson()
        {
            foreach (var lesson in _materials)
                if (lesson.GetType() == typeof(VideoMaterial))
                    return LessonType.VideoLesson;

            return LessonType.TextLesson;
        }
    }
}