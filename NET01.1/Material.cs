﻿using System;

namespace NET01._1
{
    public abstract class Material : Entity
    {
        protected Material()
        {
        }

        protected Material(string description) : base(description)
        {
        }

        protected Material(string description, Guid guid) : base(description, guid)
        {
        }

        public abstract Material Clone();
    }
}