﻿using System;

namespace NET01._1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var videoMaterial = new VideoMaterial("someImg", "someVideo", VideoFormat.Mp4, "description");
            var textMaterial = new TextMaterial("some text");
            var training = new Training();

            training.AddMaterial(videoMaterial);
            training.AddMaterial(textMaterial);

            var cloneTraining = (Training) training.Clone();

            Console.WriteLine(cloneTraining[0].Description);
            Console.WriteLine(training[0].Description);
            cloneTraining[0].Description = "new description";
            Console.WriteLine(cloneTraining[0].Description);
            Console.WriteLine(training[0].Description);

            training.SetVersion(new byte[8]{1,2,3,4,5,6,7,8});
            var mas = training.GetVersion();
            foreach (var ma in mas)
            {
                Console.WriteLine(ma);
            }

            Console.WriteLine(training.TypeOfLesson());
            Console.WriteLine(videoMaterial.ToString());
            Console.WriteLine(training.Equals(cloneTraining));
            Console.ReadLine();
        }
    }
}