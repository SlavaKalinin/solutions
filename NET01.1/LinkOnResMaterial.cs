﻿using System;

namespace NET01._1
{
    public class LinkOnResMaterial : Material
    {
        private string _uriContent;

        public LinkOnResMaterial(string uriContent, LinkType linkType)
        {
            UriContent = uriContent;
            LinkType = linkType;
        }

        public LinkOnResMaterial(string description, string uriContent, LinkType linkType) : base(description)
        {
            UriContent = uriContent;
            LinkType = linkType;
        }

        public LinkOnResMaterial(string description, string uriContent, LinkType linkType, Guid guid) : base(
            description, guid)
        {
            UriContent = uriContent;
            LinkType = linkType;
        }

        public LinkType LinkType { get; set; }

        public string UriContent
        {
            get => _uriContent;
            set
            {
                CheckForNullOrEmpty(value);
                _uriContent = value;
            }
        }

        public override Material Clone()
        {
            return new LinkOnResMaterial(Description = Description, UriContent = UriContent, LinkType = LinkType,
                Id = Id);
        }

        private void CheckForNullOrEmpty(string value)
        {
            if (string.IsNullOrEmpty(value)) throw new ArgumentException("URI content can't be null or empty");
        }
    }
}