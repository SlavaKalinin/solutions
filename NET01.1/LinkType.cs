﻿namespace NET01._1
{
    public enum LinkType
    {
        Unknown,
        Html,
        Image,
        Audio,
        Video
    }
}
