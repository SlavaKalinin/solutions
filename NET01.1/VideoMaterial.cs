﻿using System;

namespace NET01._1
{
    public class VideoMaterial : Material, IVersionable
    {
        private readonly byte[] _version = new byte[8];
        private string _uriVideo;

        public VideoMaterial(string uriVideo)
        {
            UriVideo = uriVideo;
            VideoForm = VideoFormat.Unknown;
        }

        public VideoMaterial(string uriImage, string uriVideo, VideoFormat videoForm, string description) : base(
            description)
        {
            UriImage = uriImage;
            UriVideo = uriVideo;
            VideoForm = videoForm;
        }

        public VideoMaterial(string uriImage, string uriVideo, VideoFormat videoForm, string description, Guid guid) :
            base(description, guid)
        {
            UriImage = uriImage;
            UriVideo = uriVideo;
            VideoForm = videoForm;
        }

        public string UriVideo
        {
            get => _uriVideo;
            set
            {
                CheckForNullOrEmpty(value);
                _uriVideo = value;
            }
        }

        public VideoFormat VideoForm { get; set; }
        public string UriImage { get; set; }

        public byte[] GetVersion()
        {
            var version = new byte[8];
            _version.CopyTo(version, 0);
            return version;
        }

        public void SetVersion(byte[] version)
        {
            if (version == null) throw new ArgumentException("Array is null!");
            if (version.Length != _version.Length) throw new ArgumentException("Length of version != 8");
            version.CopyTo(_version, 0);
        }

        public override Material Clone()
        {
            return new VideoMaterial(UriImage = UriImage, UriImage = UriImage, VideoForm = VideoForm,
                Description = Description, Id = Id);
        }

        private void CheckForNullOrEmpty(string value)
        {
            if (string.IsNullOrEmpty(value)) throw new ArgumentException("URI of Video can't be null or empty");
        }
    }
}