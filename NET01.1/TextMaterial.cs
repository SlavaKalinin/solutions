﻿using System;

namespace NET01._1
{
    public class TextMaterial : Material
    {
        private string _textMaterial;

        public TextMaterial(string textMaterial)
        {
            TextMat = textMaterial;
        }

        public TextMaterial(string textMaterial, string description, Guid guid) : base(description, guid)
        {
            TextMat = textMaterial;
        }

        public string TextMat
        {
            get => _textMaterial;
            set
            {
                CheckForNullAndLength(value);
                _textMaterial = value;
            }
        }

        public override Material Clone()
        {
            return new TextMaterial(TextMat = TextMat, Description = Description, Id = Id);
        }

        private void CheckForNullAndLength(string value)
        {
            if (string.IsNullOrEmpty(value)) throw new ArgumentException("TextMaterial's can't be empty!");
            if (value.Length > 10000) throw new ArgumentException("TextMaterial can't be more 10000 symbols");
        }
    }
}