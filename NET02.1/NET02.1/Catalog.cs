﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NET02._1
{
    public class Catalog : IEnumerable<Book>
    {
        private readonly Dictionary<string, Book> _books = new Dictionary<string, Book>();

        public Book this[string key]
        {
            get
            {
                key = IsbnHelper.IsbnNormalization(key);
                return _books[key];
            }
        }

        public IEnumerator<Book> GetEnumerator()
        {
          return _books.Values.OrderBy(x => x.BookName).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(Book book)
        {
            _books.Add(book.Isbn, book);
        }

        public IEnumerable<Book> GetByAuthor(string name, string surname)
        {
            Author author = new Author(name, surname);
            return _books.Select(x => x.Value).Where(x => x.Author.Contains(author));
        }

        public IEnumerable<Book> GetByDate()
        {
            return _books.Values.OrderBy(book => book.Date);
        }

        public IEnumerable<(string, int)> GetAuthorCount()
        {
            return _books.Values.Select(book => new { book.BookName, book.Author })
                .SelectMany(item => item.Author,
                    (item, author) => new { item.BookName, Author = author.ToString() })
                .GroupBy(g => g.Author)
                .Select(s => (s.Key, s.Count())).OrderBy(k => k.Item1);
        }
    }
}