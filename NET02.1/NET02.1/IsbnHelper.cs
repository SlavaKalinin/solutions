﻿using System;
using System.Text.RegularExpressions;

namespace NET02._1
{
    public static class IsbnHelper
    {
        public static void CheckIsbn(string isbn)
        {
            const string pattern = "(^((97[89])[0-9]{10}$))|(^((97[89])-[0-9]{1}-[0-9]{2}-[0-9]{6}-[0-9]{1})$)";
            Regex newReg = new Regex(pattern);
            if (!newReg.IsMatch(isbn))
            {
                throw new ArgumentException(nameof(isbn));
            }
        }

        public static string IsbnNormalization(string isbn)
        {
            return string.Join("", isbn.Split('-'));
        }
    }
}