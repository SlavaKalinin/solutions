﻿using System;

namespace NET02._1
{
    public class Author
    {
        public string FirstName { get; }

        public string LastName { get; }

        public Author(string firstName, string lastName)
        {
            CheckCorrectness(firstName);
            CheckCorrectness(lastName);
            FirstName = firstName.ToLower();
            LastName = lastName.ToLower();
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }

            if (this == obj)
            {
                return true;
            }

            if (obj.GetType() != GetType())
            {
                return false;
            }

            var another = (Author)obj;
            return string.Equals(FirstName, another.FirstName) &&
                   string.Equals(LastName, another.LastName);
        }

        public override int GetHashCode()
        {
            return (LastName + FirstName).GetHashCode();
        }

        public override string ToString()
        {
            return FirstName + " " + LastName;
        }

        private void CheckCorrectness(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (name == "")
            {
                throw new ArgumentException(nameof(name));
            }

            if (name.Length > 200)
            {
                throw new ArgumentException(nameof(name));
            }
        }
    }
}