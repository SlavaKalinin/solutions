﻿using System;
using System.Collections.Generic;

namespace NET02._1
{
    public class Program
    {
        private static void Main(string[] args)
        {
            Book book1 = new Book("1984", new List<Author> {new Author("George", "Orwell")},
                new DateTime(1948, 01, 01), "9787506261654");

            Book book2 = new Book("Animal Farm", new List<Author> {new Author("George", "Orwell")},
                new DateTime(1945, 8, 17), "9780141393056");

            Book book3 = new Book("Improper integrals", new List<Author>
            {
                new Author("Oleg", "Kastritsa"), new Author("Sergey", "Mazanik"),
                new Author("Adolf", "Naumovich"), new Author("Nil", "Naumovich")
            }, new DateTime(2011, 01, 01), "978-9-85-518996-2");

            Book book4 = new Book("Mathematical analysis", new List<Author>
            {
                new Author("Oleg", "Kastritsa"), new Author("Sergey", "Mazanik")
            }, new DateTime(2012, 01, 01), "978-9-85-518975-7");

            Book book5 = new Book("Differential equations", new List<Author>
            {
                new Author("Larisa", "Alsevich"), new Author("Sergey", "Mazanik")
            }, new DateTime(2015, 01, 01), "9781242367697");

            Catalog catalog = new Catalog {book1, book2, book3, book4, book5};

            Book book6 = new Book("Differential equations", new List<Author>
            {
                new Author("Larisa", "Alsevich"), new Author("Sergey", "Mazanik")
            }, new DateTime(2015, 01, 01), "9781242367697");
           // catalog.Add(book6); throws an argument exception, there is an element with the same key
            Console.WriteLine(catalog["9789855189757"].BookName);
            Console.WriteLine(catalog["978-9-85-518975-7"].BookName);
            Console.WriteLine(catalog["9780141393056"].BookName);
            Console.WriteLine("==================");

            foreach (Book book in catalog.GetByAuthor("George", "Orwell"))
            {
                Console.WriteLine(book.BookName);
            }

            Console.WriteLine("==================");
            foreach ((string, int) autCount in catalog.GetAuthorCount())
            {
                Console.WriteLine(autCount.Item1 + " " + autCount.Item2);
            }

            Console.WriteLine("==================");
            foreach (Book book in catalog.GetByDate())
            {
                Console.WriteLine(book.BookName + " " + book.Date.ToString("d"));
            }

            Console.WriteLine("==================");
            foreach (Book book in catalog) //order by name
            {
                Console.WriteLine(book.BookName);
            }

            Console.ReadLine();
        }
    }
}