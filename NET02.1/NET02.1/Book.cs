﻿using System;
using System.Collections.Generic;

namespace NET02._1
{
    public class Book
    {
        public DateTime Date { get; }

        public string BookName { get; }

        public string Isbn { get; }

        public List<Author> Author { get; }

        public Book(string name, List<Author> authors, DateTime date, string isbn)
        {
            IsbnHelper.CheckIsbn(isbn);
            CheckCorrectnessName(name);
            CheckCorrectnessAuthor(authors);

            BookName = name;
            Author = authors;
            Date = date;
            Isbn = IsbnHelper.IsbnNormalization(isbn);
        }

        private void CheckCorrectnessName(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (name == "")
            {
                throw new ArgumentException(nameof(name));
            }

            if (name.Length > 1000)
            {
                throw new ArgumentException(nameof(name));
            }
        }

        private void CheckCorrectnessAuthor(List<Author> authors)
        {
            if (authors == null)
            {
                throw new ArgumentNullException(nameof(authors));
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }

            if (this == obj)
            {
                return true;
            }

            if (obj.GetType() != GetType())
            {
                return false;
            }

            Book another = (Book)obj;
            return another.Isbn.Equals(Isbn);
        }

        public override int GetHashCode()
        {
            return (int)long.Parse(Isbn);
        }
    }
}