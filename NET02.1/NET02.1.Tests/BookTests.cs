﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NET02._1.Tests
{
    [TestClass]
    public class BookTests
    {
        [TestMethod]
        public void Isbn_Equals_true()
        {
            Book book1 = new Book("A", new List<Author> {},
                new DateTime(2000, 01, 01), "9787506261654");
            Book book2 = new Book("1984", new List<Author> { new Author("George", "Orwell") },
                new DateTime(1948, 01, 01), "978-7-50-626165-4");
            Assert.AreEqual(book1, book2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Isbn_Set_SetWithWrongArg()
        {
            Book book1 = new Book("A", new List<Author> { },
                new DateTime(2000, 01, 01), "9787554");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void BookName_Set_LengthMore1000()
        {
            Book book1 = new Book(new string('a',1001), new List<Author> { },
                new DateTime(2000, 01, 01), "9787506261654");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void BookName_Set_Null()
        {
            Book book1 = new Book(null, new List<Author> { },
                new DateTime(2000, 01, 01), "9787506261654");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void BookName_Set_Empty()
        {
            Book book1 = new Book("", new List<Author> { },
                new DateTime(2000, 01, 01), "9787506261654");
        }
    }
}