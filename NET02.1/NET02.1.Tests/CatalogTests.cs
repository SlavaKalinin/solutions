﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NET02._1.Tests
{
    [TestClass]
    public class CatalogTests
    {
        [TestMethod]
        public void GetByKey_True()
        {
            Book book = new Book("Name", new List<Author> {new Author("A", "B")},
                new DateTime(2000, 01, 01), "9781242367697");
            Catalog catalog = new Catalog {book};
            Assert.AreEqual(catalog["9781242367697"].BookName, book.BookName);
        }
        [TestMethod]
        public void GetByKey_WithDiffIsbnForm_True()
        {
            Book book = new Book("Name", new List<Author> { new Author("A", "B") },
                new DateTime(2000, 01, 01), "978-1-24-236769-7");
            Catalog catalog = new Catalog { book };
            Assert.AreEqual(catalog["9781242367697"].BookName, book.BookName);
        }
    }
}