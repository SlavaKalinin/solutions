﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NET02._1.Tests
{
    [TestClass]

    public class AuthorTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void AuthorFirstName_Set_LengthMore200()
        {
            Author author = new Author(new string('a', 201), "something");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AuthorFirstName_Set_Null()
        {
           Author author = new Author("smth", null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void AuthorFirstName_Set_Empty()
        {
           Author author = new Author("", "smth");
        }
    }
}