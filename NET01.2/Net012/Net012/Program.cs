﻿using System;

namespace Net012
{
    public class Program
    {
        private static void Main(string[] args)
        {
            var rnd = new Random();
            var matrices = new SquareMatrix<int>[2];
            matrices[0] = new SquareMatrix<int>(3);
            matrices[1] = new DiagonalMatrix<int>(3);
            foreach (var matrix in matrices)
            {
                for (var i = 0; i < matrix.Size; i++)
                {
                    for (var j = 0; j < matrix.Size; j++)
                    {
                        matrix[i, j] = rnd.Next(0, 10);
                    }
                }
            }

            // matrices[0][3, 2] = 5;

            foreach (var matrix in matrices)
            {
                Console.WriteLine(matrix.ToString());
            }


            var squareMatrix = new SquareMatrix<int>(2) {[0, 0] = 1, [0, 1] = 2, [1, 0] = 3, [1, 1] = 4};
            Console.WriteLine(squareMatrix.ToString());

            squareMatrix.ElementChanged += delegate { Console.WriteLine("Changed"); };
            squareMatrix.ElementChanged += (o, e) => Console.WriteLine("Changed");
            squareMatrix.ElementChanged += ElementChanged;
            squareMatrix[1, 1] = 2;
            squareMatrix[1, 0] = 3;
            Console.WriteLine(squareMatrix.ToString());


            Console.ReadLine();
        }

        private static void ElementChanged(object sender, ElementChangedEventArgs<int> e)
        {
            Console.WriteLine($"[{e.I},{e.J}] was {e.Old}, now {e.New}");
        }
    }
}