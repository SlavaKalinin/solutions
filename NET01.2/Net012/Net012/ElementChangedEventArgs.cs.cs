﻿using System;

namespace Net012
{
    /// <summary>
    ///     This class presents helpful class for events
    /// </summary>
    /// <typeparam name="T">All types can be used</typeparam>
    public class ElementChangedEventArgs<T> : EventArgs
    {
        /// <summary>
        ///     Constructor creates object of helpful class
        /// </summary>
        /// <param name="i">line</param>
        /// <param name="j">column</param>
        /// <param name="old">old value</param>
        /// <param name="newV">new value</param>
        public ElementChangedEventArgs(int i, int j, T old, T newV)
        {
            I = i;
            J = j;
            Old = old;
            New = newV;
        }

        /// <value>Returns line</value>
        public int I { get; }

        /// <value>Returns column</value>
        public int J { get; }

        /// <value>Returns old value</value>
        public T Old { get; }

        /// <value>Returns new value</value>
        public T New { get; }
    }
}