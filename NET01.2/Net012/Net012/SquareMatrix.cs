﻿using System;
using System.Text;

namespace Net012
{
    /// <summary>
    ///     This class presents square matrix
    /// </summary>
    /// <typeparam name="T"> All types can be stored in matrix</typeparam>
    public class SquareMatrix<T>
    {
        /// <value>Size of matrix</value>
        private int _size;

        ///<value>Returns size of matrix</value>
        protected T[] Data;

        /// <summary>
        ///     Constructor, which creates square matrix.
        /// </summary>
        /// <param name="size">size of matrix</param>
        public SquareMatrix(int size)
        {
            Size = size;
            Data = new T[size * size];
        }

        /// <value>Size of matrix</value>
        /// <exception cref="ArgumentException"></exception>
        /// <paramref name="value" />
        /// Size of matrix must be > 0
        public int Size
        {
            get => _size;
            protected set
            {
                if (value < 0)
                {
                    throw new ArgumentException("size < 0");
                }

                _size = value;
            }
        }

        /// <summary>
        ///     Indexer, it gives access to an element from matrix
        /// </summary>
        /// <param name="i">Line</param>
        /// <param name="j">Column</param>
        /// <returns>Returns an element from i-line, j-column</returns>
        /// <see cref="ElementChangedEventArgs{T}" />
        /// Calls an event, if element has changed
        public virtual T this[int i, int j]
        {
            get
            {
                CheckIndexes(i, j);
                return Data[i * Size + j];
            }
            set
            {
                CheckIndexes(i, j);
                var oldValue = Data[i * Size + j];
                if (Equals(oldValue, value))
                {
                    return;
                }

                Data[i * Size + j] = value;
                OnElementChanged(new ElementChangedEventArgs<T>(i, j, oldValue, value));
            }
        }

        /// <summary>
        ///     Declares event
        /// </summary>
        public event EventHandler<ElementChangedEventArgs<T>> ElementChanged;

        /// <summary>
        ///     Function that calls method after event
        /// </summary>
        /// <param name="value">New value in matrix</param>
        protected void OnElementChanged(ElementChangedEventArgs<T> value)
        {
            ElementChanged?.Invoke(this, value);
        }

        /// <summary>
        ///     Checks indexes for correctness.
        /// </summary>
        /// <param name="i">Index of line.</param>
        /// <param name="j">Index of column.</param>
        /// <exception cref="IndexOutOfRangeException"></exception>
        /// <paramref name="i, j" />
        /// Indexes must belong to matrix
        protected void CheckIndexes(int i, int j)
        {
            if (i < 0 || i >= Size)
            {
                throw new IndexOutOfRangeException(nameof(i));
            }

            if (j < 0 || j >= Size)
            {
                throw new IndexOutOfRangeException(nameof(j));
            }
        }

        /// <summary>
        ///     Presents matrix in convenient form
        /// </summary>
        /// <returns>Matrix like a string</returns>
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            for (var i = 0; i < Size; i++)
            {
                for (var j = 0; j < Size; j++)
                {
                    stringBuilder.Append(this[i, j] + " ");
                }

                stringBuilder.Append("\n");
            }

            return stringBuilder.ToString();
        }
    }
}