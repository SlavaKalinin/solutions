﻿namespace Net012
{
    /// <summary>
    ///     This class presents diagonal matrix
    ///     diagonal matrix stores element only on main diagonal, other elements are zeros
    /// </summary>
    /// <typeparam name="T"> All types can be stored in matrix</typeparam>
    public class DiagonalMatrix<T> : SquareMatrix<T>
    {
        /// <summary>
        ///     Constructor, which creates square matrix.
        /// </summary>
        /// <param name="size">size of matrix</param>
        public DiagonalMatrix(int size) : base(size)
        {
            Data = new T[Size];
        }

        /// <summary>
        ///     Indexer, it gives access to an element from matrix
        /// </summary>
        /// <param name="i">Line</param>
        /// <param name="j">Column</param>
        /// <returns>Returns an element from i-line, j-column</returns>
        /// <see cref="ElementChangedEventArgs{T}" />
        /// Calls an event, if element has changed
        public override T this[int i, int j]
        {
            get
            {
                CheckIndexes(i, j);
                return i != j ? default : Data[i];
            }
            set
            {
                CheckIndexes(i, j);
                if (i != j)
                {
                    return;
                }

                var oldValue = Data[i];
                if (Equals(oldValue, value))
                {
                    return;
                }
                Data[i] = value;
                OnElementChanged(new ElementChangedEventArgs<T>(i, j, oldValue, value));
            }
        }
    }
}