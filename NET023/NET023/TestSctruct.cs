﻿using LoggerDll;

namespace NET023
{
    [TrackingEntity]
    public struct TestStruct
    {
        [TrackingProperty("group")] private int _group;

        [TrackingProperty("Age")] public int Age { get; set; }
        [TrackingProperty("Name")] public string Name { get; set; }
        public bool IsStudent { get; set; }

        public TestStruct(int group)
        {
            _group = group;
            Age = 0;
            Name = null;
            IsStudent = false;
        }
    }
}