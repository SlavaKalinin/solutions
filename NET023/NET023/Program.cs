﻿using System;
using LoggerDll;

namespace NET023
{
    public class Program
    {
        private static void Main(string[] args)
        {
            Logger logger = new Logger();
            logger.Debug("message");
            logger.Info("another message");
            // logger.Trace("some mess"); throws exception cause of minLevel

            foreach (Tuple<object, object> item in logger.Track(new TestClass(10)
                {Age = 1, IsStudent = true, Name = "name"}))
            {
                Console.WriteLine(item.Item1 + " " + item.Item2);
            }

            Console.ReadLine();
        }
    }
}