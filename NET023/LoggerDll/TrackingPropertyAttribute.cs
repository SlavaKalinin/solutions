﻿using System;

namespace LoggerDll
{
    public class TrackingPropertyAttribute : Attribute
    {
        public string Name { get; set; }

        public TrackingPropertyAttribute(string name)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

    }
}