﻿using System;
using System.IO;
using Listener;
using Microsoft.Office.Interop.Word;

namespace WordListener
{
    public class Word : IEntityListener
    {
        public string Path { get; }
        public string FileName { get; }
        public string Fontsize { get; }
        public string Shadow { get; }
        public string Fontname { get; }

        public Word(WordConfig args)
        {
            CheckArgs(args);

            Path = args.Path;
            FileName = args.FileName;
            Fontsize = args.Fontsize;
            Shadow = args.Shadow;
            Fontname = args.Fontname;
        }

        public void WriteInfo(string logLevelName, string strFunc)
        {
            CheckArgs(strFunc);
            CheckArgs(logLevelName);

            CreateDir();

            Application application = new Application();

            Document doc = application.Documents.Open($@"{System.IO.Path.GetFullPath(Path)}\{FileName}.docx");

            Paragraph a = doc.Paragraphs.Add();
            a.Range.Text = DateTime.Now.ToString("G") + $" {logLevelName} " + strFunc;
            a.Range.Font.Size = float.Parse(Fontsize);
            a.Range.Font.Name = Fontname;
            a.Range.Font.Shadow = int.Parse(Shadow);
            doc.Paragraphs.Add();

            doc.Save();
            doc.Close();
            application.Quit();


        }

        private void CreateDir()
        {
            if (!File.Exists($@"{System.IO.Path.GetFullPath(Path)}{FileName}.docx"))
            {
                Application application = new Application();
                Document doc = application.Documents.Add();
                doc.SaveAs($@"{System.IO.Path.GetFullPath(Path)}{FileName}.docx");
                doc.Close();
                application.Quit();
            }
        }

        private void CheckArgs(object arg)
        {
            if (arg is null)
            {
                throw new ArgumentNullException(nameof(arg));
            }
        }
    }
}