﻿using System;
using Listener;

namespace WindowListener
{
    public class WindowConfig : EntityListenerConfig
    {
        private string _id;
        private string _category;

        public string Category
        {
            get => _category;
            set
            {
                CheckArgs(value);
                _category = value;
            }
        }

        public string Id
        {
            get => _id;
            set
            {
                CheckArgs(value);
                _id = value;
            }
        }

        public WindowConfig(string id, string category)
        {
            Id = id;
            Category = category;
        }

        public WindowConfig() : this("0", "0") { }

        private void CheckArgs(object arg)
        {
            if (arg is null)
            {
                throw new ArgumentNullException(nameof(arg));
            }
        }
    }
}