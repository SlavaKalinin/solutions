﻿using System;
using System.Diagnostics;
using Listener;

namespace WindowListener
{
    public class Window : IEntityListener
    {
        public string Id { get; }
        public string Category { get; }

        public Window(WindowConfig args)
        {
            CheckArgs(args);

            Id = args.Id;
            Category = args.Category;
        }

        public void WriteInfo(string logLevelName, string message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message), "Message is null");
            }

            using (EventLog eventLog = new EventLog("Application"))
            {
                eventLog.Source = "Application";
                switch (logLevelName)
                {
                    case "Debug":
                        eventLog.WriteEntry(message, EventLogEntryType.SuccessAudit, int.Parse(Id), short.Parse(Category));
                        break;
                    case "Info":
                        eventLog.WriteEntry(message, EventLogEntryType.Information, int.Parse(Id), short.Parse(Category));
                        break;
                    case "Warn":
                        eventLog.WriteEntry(message, EventLogEntryType.Warning, int.Parse(Id), short.Parse(Category));
                        break;
                    case "Error":
                        eventLog.WriteEntry(message, EventLogEntryType.Error, int.Parse(Id), short.Parse(Category));
                        break;
                    case "Fatal":
                        eventLog.WriteEntry(message, EventLogEntryType.FailureAudit, int.Parse(Id), short.Parse(Category));
                        break;
                }
            }
        }

        private void CheckArgs(object arg)
        {
            if (arg is null)
            {
                throw new ArgumentNullException(nameof(arg));
            }
        }
    }
}
