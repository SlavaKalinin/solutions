﻿using System;
using Listener;

namespace TextListener
{
    public class TextConfig : EntityListenerConfig
    {
        private string _filename;
        private string _path;

        public string Path
        {
            get => _path;
            set
            {
                CheckArgs(value);
                _path = value;
            }
        }

        public string FileName
        {
            get => _filename;
            set
            {
                CheckArgs(value);
                _filename = value;
            }
        }

        public TextConfig() : this(" ", " ")
        {
        }

        public TextConfig(string path, string filename)
        {
            Path = path;
            FileName = filename;
        }

        private void CheckArgs(object arg)
        {
            if (arg is null)
            {
                throw new ArgumentNullException(nameof(arg));
            }
        }
    }
}