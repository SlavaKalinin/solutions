﻿using System;
using System.IO;
using System.Text;
using Listener;

namespace TextListener
{
    public class Text : IEntityListener
    {
        public string Path { get; }
        public string FileName { get; }

        public Text(TextConfig args)
        {
            CheckArgs(args);

            Path = args.Path;
            FileName = args.FileName;
        }

        public void WriteInfo(string strFunc, string strInfo)
        {
            CreateDir();
            using (StreamWriter sw = new StreamWriter
                ($@"{System.IO.Path.GetFullPath(Path)}{FileName}.txt", true, Encoding.Default))
            {
                sw.WriteLine(DateTime.Now.ToString("G") + $" {strInfo} " + strFunc);
            }
        }

        private void CreateDir()
        {
            if (File.Exists($@"{System.IO.Path.GetFullPath(Path)}{FileName}.txt")) return;
            using (File.Create($@"{System.IO.Path.GetFullPath(Path)}{FileName}.txt"))
            {
            }
        }

        private void CheckArgs(object arg)
        {
            if (arg is null)
            {
                throw new ArgumentNullException(nameof(arg));
            }
        }
    }
}