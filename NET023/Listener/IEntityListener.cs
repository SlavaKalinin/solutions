﻿namespace Listener
{
    public interface IEntityListener
    {
        void WriteInfo(string strFunc, string strInfo);
    }
}